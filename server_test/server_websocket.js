
var ws = require("nodejs-websocket");
var ip = require("ip");
var devicePlatform = {
    'Android': 1,
    'SmarTV': 2,
    'iOS': 3,
    'Windows': 4,
    'Linux': 5,
    'Chrome': 6
};
var commandsName = {
    message: "message",
    error: "error",
    event: "event",
    webSocket: {
        discoveryServers: {
            request: "ws_req_discoveryServers",
            response: "ws_res_discoveryServers"
        },
        updateDevicesInfo: {
            request: "ws_req_updateDevicesInfo",
            response: "ws_res_updateDevicesInfo"
        },
        getUniqueId: {
            request: "ws_req_getUniqueId",
            response: "ws_res_getUniqueId"
        },
        sendDevice: "ws_sendDevice"
    }
};

var lastUniqueId = 1;
var listedDevices = [];
var serverDevice = {};
serverDevice.uniqueId = lastUniqueId++;
serverDevice.friendlyName = 'Large Screen Surface - Server 01';
serverDevice.platform = devicePlatform.Chrome;
serverDevice.ipAddress = ip.address();
serverDevice.latitude = 0;
serverDevice.longitude = 0;

var server = ws.createServer(function (socket) {
    // Auto-incrementar el uniqueId.
    socket.uniqueId = lastUniqueId++;
    socket.device = null;

    sendCommand(socket, {
        "command": commandsName.webSocket.getUniqueId.response,
        "customData": socket.uniqueId
    });

    socket.on("text", function (object) {
        console.log('RECEIVE: ' + object);

        parseCommand(object, socket);
    });

    socket.on("close", function () {
        listedDevices.pop();
        broadcastCommand({
            "command": commandsName.webSocket.updateDevicesInfo.response,
            "customData": listedDevices
        });

    });
});

/**
 * Identifica qué comando debe ser ejecutado por el
 * mensaje enviado desde un cliente.
 *
 * @param object
 */
var parseCommand = function (object, socket) {
    object = JSON.parse(object);

    if ('command' in object) {
        switch (object.command) {
            case 'webRTC_fileInfo':
                broadcasCommandExceptOne(object, object.owner.uniqueId);
                break;

            case 'webRTC_signal':
                unicastCommand(object, object.targetDevice.uniqueId);
                break;

            case commandsName.webSocket.sendDevice:
                var connectedDevices = [];
                listedDevices = [];
                server.connections.forEach(function (s) {
                    if (s.uniqueId == object.device.uniqueId)
                        s.device = object.device;

                    if (s.device) {
                        connectedDevices.push(s.device);
                        listedDevices.push(s.device);
                    }
                });

                // Actualizar la lista de dispositivos en todos los clientes.
                broadcastCommand({
                    "command": "webSocket_updateConnectedDevices",
                    "connectedDevices": connectedDevices
                });
                break;

            case commandsName.webSocket.discoveryServers.request:
                sendCommand(socket, {
                    command: commandsName.webSocket.discoveryServers.response,
                    status: "avaliable"
                });
                socket.close();
                break;

            case commandsName.message:
                if (object.targetDevices) {
                    for (var i = 0; i < object.targetDevices.length; i++) {
                        var device = object.targetDevices[i];
                        unicastCommand(object, device.uniqueId);
                    }
                }
                else
                    broadcasCommandExceptOne(object, object.from.uniqueId);
                break;

            case commandsName.event:
                broadcasCommandExceptOne(object, object.from.uniqueId);
                break;
        }
    }
};

/**
 * Enviar un comando a todos los clientes conectados.
 *
 * @param object
 */
var broadcastCommand = function (object) {
    server.connections.forEach(function (s) {
        sendCommand(s, object);
    });
};

/**
 *
 * @param object
 * @param unique_id
 */
var broadcasCommandExceptOne = function (object, unique_id) {
    server.connections.forEach(function (s) {
        if (s.uniqueId != unique_id)
            sendCommand(s, object);
    });
};

/**
 * Enviar un comando a un único cliente utilizando el unique ID.
 *
 * @param object
 * @param unique_id
 */
var unicastCommand = function (object, unique_id) {
    server.connections.forEach(function (s) {
        if (s.uniqueId == unique_id)
            sendCommand(s, object);
    });
};

/**
 * Enviar un comando a un cliente en específico utilizando el socket.
 *
 * @param socket
 * @param object
 */
var sendCommand = function (socket, object) {
    console.log('SEND: ' + JSON.stringify(object));

    socket.sendText(JSON.stringify(object));
};

var port = 8051;

server.listen(port);
console.log('Servidor Backend levantado! Escuchando el puerto: 8051');