var lsf = (function (configOptions) {

  //// FUNCIONES PRIVADAS  ////
  var _lsf = {
    debug: true,
    /** Permite gestionar la trazabilidad y seguimiento del programa */
    log: {
      /** Emite un log en la consola */
      trace: function () {
        if (_lsf.debug) {
          console.log.apply(console, arguments);
        }
      },
      /** Emite un mensaje de informacion en la consola */
      info: function () {
        if (_lsf.debug) {
          console.info.apply(console, arguments);
        }
      },
      /** Emite un mensaje de alerta en la consola */
      warn: function () {
        if (_lsf.debug) {
          console.warn.apply(console, arguments);
        }
      },
      /** Emite un mensaje de error en la consola */
      error: function () {
        if (_lsf.debug) {
          console.error.apply(console, arguments);
        }
      }
    },

    /** Lista de los nombres de los comandos a recibir y enviar */
    commandsName: {
      message: "message",
      error: "error",
      event: "event",
      webSocket: {
        discoveryServers: {
          request: "ws_req_discoveryServers",
          response: "ws_res_discoveryServers"
        },
        updateDevicesInfo: {
          request: "ws_req_updateDevicesInfo",
          response: "ws_res_updateDevicesInfo"
        },
        getUniqueId: {
          request: "ws_req_getUniqueId",
          response: "ws_res_getUniqueId"
        },
        sendDevice: "ws_sendDevice"
      }
    },
    /** Modulo de Conectividad de WebSockets */
    webSocket: {
      /** Objecto de conexion */
      connection: null,
      /** Configuracion de la conexion */
      connectionConfig: {
        host: "localhost", // host al que se va a conectar,
        usingSecure: false, // flag que activa o desactiva el uso del protocolo ws o wss segun sea el caso
        protocol: {
          secure: "wss://", //cuando el servidor usa un protocolo seguro
          insecure: "ws://", //cuando el servidor usa un protocolo inseguro
        },
        port: 8080, //puerto por el cual se va a establecer la conexion
        keepAlive: 5 //segundos entre los cuales se manda una señal para mantener la conexion activa
      },
      /** Intenta establecer una conexión con el Servidor en base a una configuración predeterminada */
      establishConnection: function (config, success, error, close) {
        var address;
        if (config.usingSecure) {
          address = config.protocol.secure + config.host + config.port;
        } else {
          address = config.protocol.insecure + config.host + config.port;
        }
        try {
          _lsf.log.info("Intentando establecer conexion con el servidor...");
          _lsf.webSocket.connection = new webSocket(address);
        } catch (err) {
          _lsf.log.error("No se pudo establecer la conexion con el servidor");
          _lsf.log.trace(err);
          if (typeof error == 'function') {
            error(err);
          }
        }
        if (_lsf.webSocket.connection !== null) {
          _lsf.webSocket.connection.established = false;
          _lsf.webSocket.connection.onopen = function (event) {
            _lsf.log.trace(event);
            _lsf.log.info("Se ha establecido la conexion con el servidor");
            _lsf.webSocket.openConnection(success, error, close);
            if (_lsf.config.keepAlive > 0) {
              var interval = setInterval(function () {
                if (!lsf.connectivity.webSocketIsActive) {
                  clearInterval(interval);
                } else {
                  _lsf.webSocket.connection.send("keepAlive");
                }
              },
                _lsf.config.keepAlive * 1000);
            }
          };
          _lsf.webSocket.connection.onerror = function (err) {
            _lsf.log.error(err);
            if (typeof error == 'function') {
              error(err);
            }
          }
          _lsf.webSocket.connection.onclose = function (event) {
            _lsf.log.info("Se ha cerrado la conexion con el servidor");
            _lsf.log.trace(event);
            if (typeof close == 'function') {
              close(event);
            }
          }
        }
      },
      /** Funcion que se ejecuta al establecerse correctamente la conexion con el servidor */
      openConnection: function (success, error, close) {
        _lsf.webSocket.established = true;
        _lsf.connection.onerror = function (err) {
          _lsf.error(err);
        };
        _lsf.connection.onclose = function (event) {
          _lsf.log.info("Se ha cerrado la conexion con el servidor");
          _lsf.log.trace(event);
          if (typeof close == 'function') {
            close(event);
          }
        };
        _lsf.connection.onmessage = function (serializedData) {
          var plot = JSON.parse(serializedData);
          if (plot != null || plot.command != null) {

          } else {
            _lsf.log.error("El commando no es compatible con la aplicación");
            if (typeof error == 'function') {
              error(new Error("Command not compatible"));
            }
          }
        };
      },
      /** Herramientas de ayuda para WebSockets */
      tools: {
        localIPReady: false,
        /** Permite localizar la ip local mediante un pedido STUN de WebRTC */
        findLocalIP: function (success, error) {
          var ip_dups = {}; //ip's duplicadas

          // Compatibilidad con firefox y chrome
          var RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
          var useWebKit = !!window.webkitRTCPeerConnection;

          // Requerimientos minimos para la conexion
          var mediaConstraints = {
            optional: [{
              RtpDataChannels: true
            }]
          };

          var servers = {
            iceServers: [{
              urls: "stun:stun.services.mozilla.com"
            }]
          };

          //Construir una nuevo RTCPeerConnection
          var pc = new RTCPeerConnection(servers, mediaConstraints);

          function handleCandidate(candidate) {
            // Encontrar la direccion IP
            var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
            if (ip_regex.exec(candidate) !== null) {
              var ip_addr = ip_regex.exec(candidate)[1];
              // Remover duplicados
              if (ip_dups[ip_addr] === undefined) {
                _lsf.log.trace("IP Local encontrada: " + ip_addr);
                _lsf.tools.localIPReady = true;
                if (typeof success == 'function') {
                  success(ip_addr);
                }
              }
              ip_dups[ip_addr] = true;
            } else {
              _lsf.log.error("Ha habido un error");
              if (typeof error == 'function') {
                error();
              }
            }
          }

          // Escuchar los eventos candidatos
          pc.onicecandidate = function (ice) {
            // Saltar los eventos no candidatos
            if (ice.candidate) {
              handleCandidate(ice.candidate.candidate);
            }
          };

          // Crear un DataChannel
          pc.createDataChannel("");

          // Crear una oferta sdp
          pc.createOffer(function (result) {
            // Disparar el request del server stun
            pc.setLocalDescription(result, function () { }, function () { });
          }, function () { });

          //Esperar a que se ejecute el pedido
          setTimeout(function () {
            //Leer la informacion del candidato desde la descripcion local
            var lines = pc.setLocalDescription.sdp.split('\n');
            lines.forEach(function (line) {
              if (line.indexOf('a=candidate:') === 0) {
                handleCandidate(line);
              }
            });
          }, 1000);
        },
        avaliableServers: [],
        /** Permite mediante una IP Local, buscar en toda su red un servidor WebSocket compatible */
        discoveryServers: function (config, success) {
          _lsf.webSocket.tools.avaliableServers = [];
          var serversPool = [];
          var splittedIP = config.host.split(".", 3);
          var mask = splittedIP[0] + "." + splittedIP[1] + "." + splittedIP[3];
          for (var i = 2; i < 255; i++) {
            try {
              var address = null
              if (config.usingSecure) {
                address = config.protocol.secure + mask + "." + i + config.port;
              } else {
                address = config.protocol.insecure + mask + "." + i + config.port;
              }
              var ws = new WebSocket(address);
              serversPool.push(ws);
              serversPool[i - 2].onopen = function () {
                _lsf.log.info("Se ha encontrado un nuevo servidor en " + this.url);
                _lsf.log.trace("Enviando peticion de descubrimiento...");
                var discoveryPlot = JSON.stringify({ command: _lsf.commandsName.webSocket.discoveryServers.request });
                this.send(discoveryPlot);
                _lsf.log.info("Enviado: " + discoveryPlot);
              }
              serversPool[i - 2].onmessage = function (serializedData) {
                var decodePlot = JSON.parse(serializedData.data);
                _lsf.log.info("Recibido: " + decodePlot);
                if (decodePlot.command == _lsf.commandsName.webSocket.discoveryServers.response) {
                  var url = this.url;
                  var serverCandidate = {
                    url: url,
                    status: decodePlot.status
                  }
                  _lsf.webSocket.tools.avaliableServers.push(serverCandidate);
                }
              }
            } catch (err) {
              _lsf.log.error(err);
            }

          }
        }
      } //fin de tools
    },
    /** Modulo de Conectividad de WebRTC */
    webRTC: {

    },
    /** Modulo de Reconocimiento de Voz - uso de la librería Annyang.js */
    speechRecognition: {

    },
    /** Modulo de Reconocimiento de Gestos */
    gestures: {

    },
    /** Modulo de Gestion de Espacios de Trabajo */
    workspaces: {

    }
  };

  //// FUNCIONES PUBLICAS ////
  /** 
   * Funciones externas del framework, con las que el usuario puede interactuar, esta dividido en 4 modulos:
   * <b>Conectividad:</b> Brinda la capacidad de conexión con otros dispositivos, actua internamente con WebSockets y con WebRTC
   * <p/><b>Reconocimiento de Voz:</b> Gracias a la implementacion interna de la libreria de Annyang.js se dota de capacidad de poder ejecutar comandos a traves de la voz
   * <p/><b>Gestos:</b> Brinda la parte de interaccion vía getos predefinidos por el framework
   * <p/><b>Espacios de Trabajo:</b> Brinda la parte de interaccion a multiples usuarios, estableciendo areas especificas para cada persona en la superficie de trabajo
   * @namespace lsf 
   */
  return {
    /**
     * Modulo de Conectividad, donde se instancian los componentes de WebSocket y WebRTC 
     * @namespace lsf.conectivity
     */
    connectivity: {
      /**
       * Verifica si la conexion esta activa
       * @returns {boolean<true|false>}
       */

      webSocketIsActive: function () {
        return _lsf.webSocket.connection != null && _lsf.webSocket.connection.established;
      },
      connect: function (success, error) {

      },
      disconnect: function (status) {

      },
      discoveryServers: function (ondiscoveryServer, options) {
        var config = $.clone(_lsf.webSocket.connectionConfig);
        if (typeof location === 'undefined' || location.protocol !== 'https:') {
          if (options == undefined) { options = {}; }
          options.usingSecure = false;
        }
        $.extend(config, options);
        try {
          _lsf.webSocket.discoveryServers(ondiscoveryServer, config);
        } catch (err) {
          _lsf.log.error("No se pudo realizar la operacion");
        }
      },
      send: {
        message: function (message, targetDevices) {

        },
        error: function (stackTrace, dataError, targetDevices) {

        },
        event: function (eventName, dataEvent, targetDevices) {

        }
      }
    }
  };
})();

(function () {
  window.lsf = lsf;
})();


/*
 * REGISTRO DE CUSTOM ELEMENTS HTML5 DEL FRAMEWORK
 */

document.registerElement('lsf-button-float');
document.registerElement('lsf-button');
document.registerElement('lsf-button-flat');
document.registerElement('lsf-card');
document.registerElement('lsf-modal');
document.registerElement('lsf-modal-dialog');
document.registerElement('lsf-tabs');
document.registerElement('lsf-tab');
document.registerElement('lsf-switch');
document.registerElement('lsf-notification');

$("lsf-button").each(function () {
  $(this).addClass("palette-" + $(this).attr("color") + " bg");
});

function toggleModal(id) {
  if ($(id).is(":visible")) {
    $(id).fadeOut(500);
  } else {
    $(id).fadeIn(500);

    setTimeout(function () {
      $(id + " > lsf-modal-dialog").css("height", $(id + " > lsf-modal-dialog").outerHeight() + "px");
      $(id + " > lsf-modal-dialog").css("bottom", "0");
    }, 500);
  }
}
