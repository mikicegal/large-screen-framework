Element.prototype.remove = function () {
    this.parentElement.removeChild(this);
};
NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};

Element.prototype.getOffsetX = function() {
    var r = document.body.getBoundingClientRect(),
        e = this.getBoundingClientRect();

    return e.left - r.left;
};
Element.prototype.getOffsetY = function() {
    var r = document.body.getBoundingClientRect(),
        e = this.getBoundingClientRect();

    return e.top - r.top;
};

Element.prototype.addClass = function (value) {
    if (this.className.indexOf(value) === -1)
        this.className += " " + value;
};
Element.prototype.removeClass = function (value) {
    this.className = this.className.replace(value, '').replace('  ', ' ').trim();
};

Element.prototype.getCssTop = function () {
    return parseInt(window.getComputedStyle(this, null).getPropertyValue('top'), 10)
};
Element.prototype.getCssLeft = function () {
    return parseInt(window.getComputedStyle(this, null).getPropertyValue('left'), 10)
};

