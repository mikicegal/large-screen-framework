/*
 * REGISTRO DE CUSTOM ELEMENTS HTML5 DEL FRAMEWORK
 */

document.registerElement('lsf-button-float');
document.registerElement('lsf-button');
document.registerElement('lsf-button-flat');
document.registerElement('lsf-card');
document.registerElement('lsf-modal');
document.registerElement('lsf-modal-dialog');
document.registerElement('lsf-tabs');
document.registerElement('lsf-tab');
document.registerElement('lsf-switch');
document.registerElement('lsf-notification');

$("lsf-button").each(function () {
    $(this).addClass("palette-" + $(this).attr("color") + " bg");
});

function toggleModal(id) {
    if ($(id).is(":visible")) {
        $(id).fadeOut(500);
    }
    else {
        $(id).fadeIn(500);

        setTimeout(function () {
            $(id + " > lsf-modal-dialog").css("height", $(id + " > lsf-modal-dialog").outerHeight() + "px");
            $(id + " > lsf-modal-dialog").css("bottom", "0");
        }, 500);
    }
}

function LSF() {

    // PROPIEDADES PRIVADAS 
    
    var __debug_mode = true;
    this.setDebugMode = function(value) {
        __debug_mode = value;
    }

    var own = this;
    var __localIPReady = false;
    var __localIP = undefined;
    var __uniqueId = null;
    var __webRTC = undefined;
    var __connectedDevices = [];
    var __avaliableServers = [];
    var __webSocketCallbacks = [];

    /**
     * Tramas para el envío de comandos.
     */
    this.plots = {
        thisDevice: function () {
            return JSON.stringify({
                uniqueId: __uniqueId,
                friendlyName: own.friendlyName,
                ipAddress: __localIP,
                platform: window.navigator.platform + " | " + window.navigator.userAgent,
                longitude: own.coordinates.longitude,
                latitude: own.coordinates.latitude
            });
        },
        
        device: function (uniqueId, friendlyName, ipAddress, platform, longitude, latitude) {
            return JSON.stringify({
                uniqueId: uniqueId,
                friendlyName: friendlyName,
                ipAddress: ipAddress,
                platform: platform,
                longitude: longitude,
                latitude: latitude
            });
        },
        
        sendMessage: function (message, targetDevices) {
            return JSON.stringify({
                command: "command",
                targetDevices: targetDevices,
                messageData: message,
                from: JSON.parse(own.plots.thisDevice())
            });
        },
        throwError: function (stackTrace, dataError, targetDevices) {
            return JSON.stringify({
                command: "error",
                stackTrace: stackTrace,
                dataError: dataError,
                targetDevices: targetDevices
            });
        },
        sendEvent: function (eventName, dataEvent, targetDevices) {
            return JSON.stringify({
                command: "event",
                eventName: eventName, //sendIdFromLSS
                data: dataEvent, // {id: n}
                from: JSON.parse(own.plots.thisDevice()),
                targetDevices: targetDevices
            });
        },

        sendDevice: function (device) {
            return JSON.stringify({
                command: "webSocket_sendDevice",
                device: device
            });
        },
        
        discoveryInfo: function () {
            return JSON.stringify({
                command: "availableServers_discoveryInfo"
            });
        },
    };

    // PROPIEDADES PÙBLICAS

    /*Puerto de escucha del servidor Websocket */
    this.applicationPort = 3000;

    /*Nombre que identifica al dispositivo actual*/
    this.friendlyName = navigator.platform + " " + Date.now();
    this.coordinates = {
        longitude: 0,
        latitude: 0,
    };

    // GETTERS

    /* Devluelve la dirección IP Local del dispositivo */
    this.getLocalIP = function () {
        return __localIP;
    };
    /* Devuelve la función relacionada con el nombre brindado por callbackName de lo contrario devuelve false
     * @param callbackName Nombre del callback a encontrar
     */
    this.getSocketEventCallback = function (callbackName) {
        if (callbackName in __webSocketCallbacks) {
            return __webSocketCallbacks[callbackName];
        }
        else {
            return false;
        }
    }

    // SETTERS

    this.setsocketEventCallback = function (callbackName, functionCallback) {
        __webSocketCallbacks[callbackName] = functionCallback;
    }

    /* Devuelve la lista de todos los dispositivos conectados una vez establecida la conexión*/
    this.getlistDevices = function () {
        return __connectedDevices;
    };

    /* Devuelve la lista de todos los servidores que se han econtrado dentro de una red local */
    this.getAvaliableServers = function () {
        return __avaliableServers;
    };

    // METODOS PRIVADOS

    /*Funcion que permite realizar un pedido via WebRTC para consultar la IP Local 
     * @param callback funcion que dispara cuando se encuentra la ip
     */
    var __callLocalIP = function (callback) {
        var ip_dups = {};

        //compatibility for firefox and chrome
        var RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
        var useWebKit = !!window.webkitRTCPeerConnection;

        //bypass naive webrtc blocking using an iframe
        if (!RTCPeerConnection) {
            //NOTE: you need to have an iframe in the page right above the script tag
            //
            //<iframe id="iframe" sandbox="allow-same-origin" style="display: none"></iframe>
            //<script>...getIPs called in here...
            //
            var win = iframe.contentWindow;
            RTCPeerConnection = win.RTCPeerConnection || win.mozRTCPeerConnection || win.webkitRTCPeerConnection;
            useWebKit = !!win.webkitRTCPeerConnection;
        }

        //minimal requirements for data connection
        var mediaConstraints = {
            optional: [{
                RtpDataChannels: true
            }]
        };

        var servers = {
            iceServers: [{
                urls: "stun:stun.services.mozilla.com"
            }]
        };

        //construct a new RTCPeerConnection
        var pc = new RTCPeerConnection(servers, mediaConstraints);

        function handleCandidate(candidate) {
            //match just the IP address
            var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
            if (ip_regex.exec(candidate) !== null) {
                var ip_addr = ip_regex.exec(candidate)[1];

                //remove duplicates
                if (ip_dups[ip_addr] === undefined)
                    callback(ip_addr);

                ip_dups[ip_addr] = true;
            }
        }

        //listen for candidate events
        pc.onicecandidate = function (ice) {

            //skip non-candidate events
            if (ice.candidate)
                handleCandidate(ice.candidate.candidate);
        };

        //create a bogus data channel
        pc.createDataChannel("");

        //create an offer sdp
        pc.createOffer(function (result) {

            //trigger the stun server request
            pc.setLocalDescription(result, function () {
            }, function () {
            });

        }, function () {
        });

        //wait for a while to let everything done
        setTimeout(function () {
            //read candidate info from local description
            var lines = pc.localDescription.sdp.split('\n');

            lines.forEach(function (line) {
                if (line.indexOf('a=candidate:') === 0)
                    handleCandidate(line);
            });
        }, 1000);

    };

 
    __callLocalIP(function (ip) {
        //local IPs
        if (ip.match(/^(192\.168\.|169\.254\.|10\.|172\.(1[6-9]|2\d|3[01]))/)) {
            __localIP = ip;
            __localIPReady = true;
        }
    });



    /* CONECTIVIDAD */

    /* Descubre en base a la iplocal todos los servidores disponibles dentro de su red local
     * @param receiveServerCallback funcion que se ejecuta al recibir un servidor nuevo
     */
    this.discoverySevers = function (receiveServerCallback) {
        if (__localIPReady) {
            var serverArrayTest = [];
            var self = this;
            console.log("Comenzando descubrimiento de servers:");
            var splittedIP = __localIP.split(".", 3);
            var IPBase = splittedIP[0] + "." + splittedIP[1] + "." + splittedIP[2];
            for (var i = 2; i < 255; i++) {
                var ws = new WebSocket("ws://" + IPBase + "." + i + ":" + this.applicationPort);
                serverArrayTest.push(ws);
                serverArrayTest[i - 2].onopen = function () {
                    console.log("Se ha encontrado un server en : " + this.url);
                    console.log("Enviando petición de descubrimiento...");
                    this.send(own.plots.discoveryInfo());
                    console.log(own.plots.discoveryInfo());
                }
                serverArrayTest[i - 2].onmessage = function (plot) {
                    var decodePlot = JSON.parse(plot.data);
                    console.log("Recibiendo status del servidor... ");
                    if (decodePlot.command == "availableServers_status") {
                        var url = this.url;
                        var serverCandidate = {
                            url: url,
                            status: decodePlot.status
                        }
                        __avaliableServers.push(serverCandidate);
                        console.log("Recibiendo:" + plot.data);
                    }
                }
            }
        }
        else {
            console.log("La ip no ha sido calculada aún");
        }
    }
    
    /**
     * WebSocket
     */

    var __webSocket_initialized = false;
    var __webSocket = null;
    var __webSocket_callbacks = {};
    
    this.webSocket = {
        init: function(url, callback) {
            __webSocket = new WebSocket(url);
            __webSocket.onopen = function () {
            };
            __webSocket.onclose = function() {
                __webSocket_initialized = false;

                if (__debug_mode) {
                    console.log('webSocket.init.onclose:');
                    console.log('');
                }
            };
            __webSocket.onmessage = function (event) {
                var plot = event.data;
                var decodePlot = JSON.parse(plot);

                switch (decodePlot.command) {
                    case 'webSocket_updateUniqueId':
                        __uniqueId = decodePlot.uniqueId;
                        
                        if (__debug_mode) {
                            console.log('webSocket.init.onmessage.updateUniqueId:');
                            console.log(JSON.parse(own.plots.thisDevice()));
                            console.log('');
                        }
    
                        // Enviar los datos de este dispositivo.
                        __webSocket.send(own.plots.sendDevice(JSON.parse(own.plots.thisDevice())));
                        break;
                        
                    case 'webSocket_updateConnectedDevices':
                        __connectedDevices = decodePlot.connectedDevices;
                        if ('webSocket_updateConnectedDevices' in __webSocketCallbacks){
                            __webSocketCallbacks['webSocket_updateConnectedDevices'](decodePlot);
                        }
                        
                        if (__debug_mode) {
                            console.log('webSocket.init.onmessage.updateConnectedDevices:');
                            console.log(__connectedDevices);
                            console.log('');
                        }
                        
                        if (!__webSocket_initialized) {
                            __webSocket_initialized = true;
                            callback();
                        }

                        if (__webRTC_initialized) {
                            __webRTC_garbageCollector();
                        }
            
                        break;
    
                    case 'webRTC_fileInfo':
                        __webRTC_receiveFileInfo(decodePlot);
                        break;

                    case 'webRTC_signal':
                        __webRTC_receiveSignal(decodePlot);
                        break;

                    case 'message':
                        if (__webSocket_callbacks.hasOwnProperty('message'))
                            __webSocket_callbacks['message'](decodePlot);
                        break;

                    case 'event':
                        if (__webSocket_callbacks.hasOwnProperty('event'))
                            __webSocket_callbacks['event'](decodePlot);
                        break;

                }
            }
        },
        on: function(name, callback) {
            __webSocket_callbacks[name] = callback;
        },
        sendMessage: function(message, targetDevices, callback) {
            __webSocket.send(own.plots.sendMessage(message, targetDevices));
            
            if (callback)
                callback();
        },
        sendEvent: function(eventName, dataEvent, targetDevices, callback) {
            __webSocket.send(own.plots.sendEvent(eventName, dataEvent, targetDevices));
            
            if (callback)
                callback();
        }
    };
    
    
    /**
     * WEB RTC
     */

    /**
     * Genera un GUID con números aleatorios. La probabilidad de colisiones es mínima.
     *
     * @returns {string}
     */
    var __webRTC_generateGuid = function () {
        var s4 = function() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };
    
    var __webRTC_chunkSize = 16 * 1024;

    var __webRTC_clientPeers = [];
    var __webRTC_broadcastObject = function (object) {
        var clientPeers = __webRTC_clientPeers;
        
        for (var i in clientPeers) {
            var peer = clientPeers[i];
            var readyState = peer.sendChannel.readyState;
            if (readyState === "open") {
                peer.sendChannel.send(object);
            }
        }
    };
    
    /**
     * Contiene todos los Buffers utilizados para los archivos enviados
     * por WebRTC.
     *
     * @type {{}}
     */
    var __webRTC_buffers = {};

    /**
     * Procesa el mensaje enviado por WebRTC a un peer en específico.
     *
     * @param peer
     * @param event
     */
    var __webRTC_onMessage = function (peer, event) {
        if (__debug_mode) {
            console.log('webRTC_onMessage(): Recibiendo una parte de un archivo por WebRTC.');
            console.log(event);
            console.log(' ');
        }

        var data = event.data;
        peer.fileTransfer.fileBuffer.push(data);
        peer.fileTransfer.receivedSize += data.byteLength;
    };

    var __webRTC_checkFileReady = function(peer) {
        if (peer.fileTransfer.guid == '')
            return;

        if (peer.fileTransfer.receivedSize === peer.fileTransfer.totalSize) {
            if (__debug_mode) {
                console.log('webRTC_onMessage(): Procesando el archivo recibido.');
                console.log(' ');
            }

            var guid = peer.fileTransfer.guid;
            __webRTC_buffers[guid] = peer.fileTransfer;
            peer.fileTransfer = {
                'guid': '',
                'fieName': '',
                'fileBuffer': [],
                'receivedSize': 0,
                'totalSize': -1
            };

            var received = new Blob(__webRTC_buffers[guid].fileBuffer);
            var downloadUrl = URL.createObjectURL(received);

            // En este punto, el archivo ha sido enviado de forma exitosa.
            // Ejecutemos el callback.
            if (__webRTC_callbacks.hasOwnProperty('file'))
                __webRTC_callbacks['file']({
                    'guid': guid,
                    'downloadUrl': downloadUrl,
                    'fileName': __webRTC_buffers[guid].fileName,
                    'fileSize': __webRTC_buffers[guid].totalSize,
                    'from': peer.targetDevice
                });
        }
    };

    /**
     *
     * @param error
     */
    var __webRTC_error = function (error) {
        if (__debug_mode) {
            console.log('webRTCError(): Un error ha ocurrido.');
            console.log(error);
            console.log(' ');
        }
    };

    /**
     *
     */
    var __webRTC_success = function () {

    };

    /**
     *
     * @type {{iceServers: *[]}}
     */
    var __webRTC_servers = {
        "iceServers": [
            {"url": "stun:stun.l.google.com:19302"}
        ]
    };

    /**
     * Establece una conexión entre este dispositivo y otro dispositivo.
     *
     * @param target_device
     */
    var __webRTC_initPeerConnection = function (target_device) {
        var currentDevice = JSON.parse(own.plots.thisDevice());
        
        var peer = {
            // Los valores dentro de fileTranfer son definidos a la hora
            // de recibir un comando via WebSocket.
            'fileTransfer': {
                'fieName': '',
                'guid': '',
                'fileBuffer': [],
                'receivedSize': 0,
                'totalSize': -1
            },

            'local': null,
            'remote': null,
            'sendChannel': null,
            'receiveChannel': null,
            'targetDevice': target_device,
            'connectionEstablished': false
        };

        peer.local = new webkitRTCPeerConnection(__webRTC_servers, null);
        peer.sendChannel = peer.local.createDataChannel("lss_channel", null);
        peer.sendChannel.binaryType = 'arraybuffer';

        peer.local.onicecandidate = function (event) {
            if (event.candidate)
                __webRTC_sendSignal('local.onicecandidate.addIceCandidate', event.candidate, target_device);
        };
        peer.local.createOffer(function (desc) {
            peer.local.setLocalDescription(desc);
            __webRTC_sendSignal('local.createOffer.setRemoteDescription', desc, target_device);
        }, __webRTC_error);

        peer.sendChannel.onopen = function (event) {
            var readyState = peer.sendChannel.readyState;
            if (readyState === "open") {
                if (__debug_mode) {
                    console.log('peer.sendChannel.onopen(): El canal WebRTC se establecio con exito entre el dispositivo '
                        + currentDevice.uniqueId + ' (currentDevice) y el ' + target_device.uniqueId + ' (targetDevice).');
                    console.log(' ');
                }

                __webRTC_connectedDevices += 1;
                __webRTC_checkReady();
            }
        };

        peer.remote = new webkitRTCPeerConnection(__webRTC_servers, null);
        peer.remote.onicecandidate = function (event) {
            if (event.candidate) {
                __webRTC_sendSignal('remote.onicecandidate.addIceCandidate', event.candidate, target_device);
            }
        };
        peer.remote.ondatachannel = function (event) {
            peer.receiveChannel = event.channel;
            peer.receiveChannel.binaryType = 'arraybuffer';
            peer.receiveChannel.onmessage = function (event) {
                __webRTC_onMessage(peer, event);
            };
            
            peer.connectionEstablished = true;
        };

        __webRTC_clientPeers[target_device.uniqueId] = peer;
    };
    
    /**
     * Borrar la conexión para aquellos dispositivos que ya no están conectados.
     */
    var __webRTC_garbageCollector = function () {
        for (var i in __webRTC_clientPeers) {
            var uniqueId = i;

            var exists = false;
            for (var j in __connectedDevices)
                if (__connectedDevices[j].uniqueId == uniqueId)
                    exists = true;

            if (!exists)
                delete __webRTC_clientPeers[uniqueId];
        }
    };

    var __webRTC_sendFileInfo = function(guid, file) {
        var currentDevice = JSON.parse(own.plots.thisDevice());
        
        var object = {
            "command": "webRTC_fileInfo",
            'guid': guid,
            'name': file.name,
            'size': file.size,
            'owner': currentDevice
        };

        if (__debug_mode) {
            console.log('webRTC_sendFileInfo:');
            console.log(object);
            console.log(' ');
        }

        __webSocket.send(JSON.stringify(object));
    };

    var __webRTC_receiveFileInfo = function(object) {
        __webRTC_clientPeers[object.owner.uniqueId].fileTransfer.fileName = object.name;
        __webRTC_clientPeers[object.owner.uniqueId].fileTransfer.guid = object.guid;
        __webRTC_clientPeers[object.owner.uniqueId].fileTransfer.totalSize = object.size;
        
        if (__debug_mode) {
            console.log('webRTC_receiveFileInfo:');
            console.log(object);
            console.log(' ');
        }
        
        __webRTC_checkFileReady(__webRTC_clientPeers[object.owner.uniqueId]);
    };
    
    /**
     * Envía una señal de comunicación a un peer en específico por WebSocket.
     * Esto es necesario para poder establecer un canal de datos entre ambos nodos.
     *
     * @param action
     * @param data
     * @param targetDevice
     * @param owner
     */
    var __webRTC_sendSignal = function (action, data, targetDevice, owner) {
        var currentDevice = JSON.parse(own.plots.thisDevice());
        
        var object = {
            'command': 'webRTC_signal',
            'action': action,
            'data': data,
            'owner': owner || currentDevice,
            'targetDevice': targetDevice
        };
        
        if (__debug_mode) {
            console.log('webRTC_sendSignal:');
            console.log(object);
            console.log(' ');
        }

        __webSocket.send(JSON.stringify(object));
    };

    /**
     * Una vez recibida la señal de comunicación, esta es ejecutada.
     *
     * @param object
     */
    var __webRTC_receiveSignal = function (object) {
        var clientPeers = __webRTC_clientPeers;
        
        if (__debug_mode) {
            console.log('webRTC_receiveSignal:');
            console.log(object);
            console.log(' ');
        }
        
        var action = object.action;
        var owner = object.owner;
        var data = object.data;

        if (!clientPeers.hasOwnProperty(owner.uniqueId))
            __webRTC_initPeerConnection(owner);

        switch (action) {
            case "local.onicecandidate.addIceCandidate":
                clientPeers[owner.uniqueId].remote.addIceCandidate(new RTCIceCandidate(data), __webRTC_success, __webRTC_error);
                break;

            case "remote.onicecandidate.addIceCandidate":
                clientPeers[owner.uniqueId].local.addIceCandidate(new RTCIceCandidate(data), __webRTC_success, __webRTC_error);
                break;

            case "local.createOffer.setRemoteDescription":
                clientPeers[owner.uniqueId].remote.setRemoteDescription(data);
                clientPeers[owner.uniqueId].remote.createAnswer(function (desc) {
                    __webRTC_sendSignal('local.createAnswer.setLocalDescription', desc, owner, object.targetDevice);
                }, __webRTC_error);
                break;

            case "local.createAnswer.setLocalDescription":
                clientPeers[owner.uniqueId].remote.setLocalDescription(data);
                __webRTC_sendSignal('remote.createAnswer.setRemoteDescription', data, owner, object.targetDevice);
                break;

            case "remote.createAnswer.setRemoteDescription":
                clientPeers[owner.uniqueId].local.setRemoteDescription(data);
                break;
        }
    };

    var __webRTC_initWebRTC = function() {
        var currentDevice = JSON.parse(own.plots.thisDevice());
        var clientDevices = __connectedDevices;
        
        if (__debug_mode) {
            console.log('webRTC_initWebRTC(): Se ha iniciado el proceso para generar los canales WebRTC.');
            console.log(clientDevices);
            console.log(' ');
        }

        __webRTC_totalDevices = clientDevices.length - 1;
        
        for (var i in clientDevices) {
            var clientDevice = clientDevices[i];
            var uniqueId = clientDevice.uniqueId;

            if (uniqueId == currentDevice.uniqueId)
                continue;

            __webRTC_initPeerConnection(clientDevice);
        }

        __webRTC_checkReady();
    };
    
    var __webRTC_checkReady = function() {
        if (__webRTC_initialized)
            return;

        if (__webRTC_totalDevices == __webRTC_connectedDevices) {
            __webRTC_initialized = true;
            
            if (__debug_mode) {
                console.log('webRTC_checkReady(): Listo!');
                console.log(__webRTC_clientPeers);
                console.log(' ');
            }
            
            __webRTC_callbacks['init']();
        }
    };
    
    var __webRTC_connectedDevices = 0;
    var __webRTC_totalDevices = 0;
    
    var __webRTC_initialized = false;
    var __webRTC_callbacks = {};

    this.webRTC = {
        init: function(callback) {
            if (!__webSocket_initialized)
                throw new Error("WebSocket must be initialized.");

            __webRTC_callbacks['init'] = callback;
            __webRTC_initWebRTC();
        },

        /**
         * Esta función permite enviar un archivo a todos los nodos conectados.
         *
         * @param file
         */
        sendFile: function (file, callback) {
            var chunkSize = __webRTC_chunkSize;
            var guid = __webRTC_generateGuid();
            
            // Colocar el callback según el guid.
            __webRTC_callbacks[guid] = callback;

            if (__debug_mode) {
                console.log('webRTC.sendFile(): Enviando archivo por WebRTC.');
                console.log(file);
                console.log(' ');
            }
    
            // Enviar el tamaño final del archivo a todos los dispositivos.
            __webRTC_sendFileInfo(guid, file);
    
            var sliceFile = function (offset) {
                var reader = new window.FileReader();
                var slice = file.slice(offset, offset + chunkSize);
    
                reader.onload = (function () {
                    return function (e) {
                        if (__debug_mode) {
                            console.log('webRTC.sendFile.sliceFile(): Enviando una parte del archivo por WebRTC.');
                            console.log('Enviando ' + (offset + slice.size) + ' de ' + file.size + '.');
                            console.log(' ');
                        }
    
                        __webRTC_broadcastObject(e.target.result);
    
                        if (file.size > offset + slice.size)
                            window.setTimeout(sliceFile, 0, offset + chunkSize);
                        else {
                            if (__debug_mode) {
                                console.log('webRTC.sendFile.sliceFile(): Listo!.');
                                console.log(' ');
                            }
                            
                            __webRTC_callbacks[guid]();
                        }
                    };
                })(file);
    
                reader.readAsArrayBuffer(slice);
            };
    
            sliceFile(0);
        },
        on: function(name, callback) {
            __webRTC_callbacks[name] = callback;
        }
        
    };


    /**
     * RECONOCIMIENTO DE VOZ
     */
     
    var __speech_recognition_commands = {};

    this.speechRecognition = {
        init: function(callback) {
            jQuery.getScript("//cdnjs.cloudflare.com/ajax/libs/annyang/2.3.0/annyang.min.js", function() {
                callback();
            });
        },
        setLanguage: function(lang) {
            annyang.setLanguage(lang);
        },
        addCallBack: function(index, callback) {
            annyang.addCallback(index, callback);
        },
        addCommand: function(command, callback) {
            __speech_recognition_commands[command] = callback;
        },
        start: function() {
            annyang.addCommands(__speech_recognition_commands);
            annyang.start();
        }
    };


    /**
     * GESTOS
     */
/*
    document.body.addEventListener('touchmove', function (event) {
        event.preventDefault();
    }, false);
    */
    var __get_touches = function (event, element) {
        var touches = [];
        var event_touches = element ? event.targetTouches : event.touches;
    
        for (var i = 0; i < event_touches.length; i++) {
            if (element && element !== event.target)
                continue;
    
            touches[touches.length] = event_touches[i];
        }
    
        return touches;
    };
    
    var __add_touch_events = function (element, add_touches, remove_touches, move_touches) {
        // Cuando uno o más toques aparecen en la pantalla.
        element.addEventListener('touchstart', add_touches, false);
        element.addEventListener('touchenter', add_touches, false);
    
        // Cuando uno o más toques se mueven.
        element.addEventListener('touchmove', move_touches, false);
    
        // Cuando uno o más toques desaparecen de la pantalla.
        element.addEventListener('touchcancel', remove_touches, false);
        element.addEventListener('touchend', remove_touches, false);
        element.addEventListener('touchleave', remove_touches, false);
    };

    var __current_gesture = {
        scroll: false,
        rotate: false,
        zoom: false,
        select: {
            multiple: false
        }
    };
    
    var __gestureOnZoon = function (element, onZoom) {
        var touches = {};
    
        var timer = null, enabled = false;
        var center_point = null;
        var last_points = null;
    
        var get_points = function() {
            var points = [];
            for (var i = 0; i < touches.length; i++)
                points.push({
                    pageX: touches[i].pageX,
                    pageY: touches[i].pageY
                });
    
            return points;
        };
        var calculate_diff = function () {
            var distance = function(a, b) {
                // Distance = √[(x1 − x0) ^ 2 + (y1 − y0) ^ 2]
                var distance = 0.0;
                distance += Math.pow(b.pageX - a.pageX, 2);
                distance += Math.pow(b.pageY - a.pageX, 2);
                distance = Math.sqrt(distance);
        
                return distance;
            };
            
            var a = last_points;
            var b = get_points();
    
            var d1 = distance(a[0], center_point) + distance(a[1], center_point); 
            var d2 = distance(b[0], center_point) + distance(b[1], center_point); 
            
            var diff = d2 - d1;
            return diff;
        };
        var clear = function() {
            window.clearTimeout(timer);
            
            center_point = null;
            last_points = null;
            enabled = false;
    
            __current_gesture.zoom = false;
        };
    
        var add_touches = function (event) {
            touches = __get_touches(event, element);
            if (touches.length != 2)
                return clear();
    
            timer = window.setTimeout(function() {
                enabled = true;
            }, 200);
    
            last_points = get_points();
    
            center_point = {
                pageX: (last_points[0].pageX + last_points[1].pageX) / 2.0,
                pageY: (last_points[0].pageY + last_points[1].pageY) / 2.0
            };
        };
        var remove_touches = function (event) {
            // Reiniciar todos los valores.
            clear();
        };
        var move_touches = function (event) {
            if (__current_gesture.scroll || __current_gesture.rotate)
                return clear();
    
            if (last_points == null)
                return clear();
    
            if (!enabled)
                return false;
    
            touches = __get_touches(event, element);
            if (touches.length != 2)
                return clear();
    
            __current_gesture.zoom = true;
    
            var diff_s = calculate_diff();
            last_points = get_points();
            
            onZoom(element, diff_s / 100.0);
        };
    
        __add_touch_events(element, add_touches, remove_touches, move_touches);
    };
    
    var __gestureOnSelect = function (element, onSelect) {
        var touches = {};
    
        var on_select_timer = null;
        var is_selected = function () {
            return element.dataset.hasOwnProperty('selected') && element.dataset['selected'] == '1';
        };
        var is_first_selected = function () {
            return element.dataset.hasOwnProperty('firstSelected') && element.dataset['firstSelected'] == '1';
        };
        var is_multi_select = function() {
            var selected = document.querySelectorAll('[data-selected="1"]');
            return selected.length >= 1;
        };
    
        var add_touches = function (event) {
            if (is_selected() && !__current_gesture.select.multiple)
                return;
    
            touches = __get_touches(event, element);
            if (touches.length != 1) {
                window.clearTimeout(on_select_timer);
                return;
            }
    
            if (is_multi_select() && __current_gesture.select.multiple) {
                if (is_selected()) {
                    element.dataset['selected'] = '0';
                    element.dataset['firstSelected'] = '0';
                    element.gestureEventOnDeselect(element);
                    return;
                }
                
                element.dataset['selected'] = '1';
                onSelect(element);
                return;
            }
    
            on_select_timer = window.setTimeout(function () {
                // Deselect all on first select.
                var selected = document.querySelectorAll('[data-selected="1"]');
                for (var i = 0; i < selected.length; i++) {
                    var e = selected[i];
                    
                    e.dataset['selected'] = '0';
                    e.dataset['firstSelected'] = '0';
                    e.gestureEventOnDeselect(e);
                }
                
                __current_gesture.select.multiple = true;
                element.dataset['selected'] = '1';
                element.dataset['firstSelected'] = '1';
                onSelect(element);
            }, 500);
        };
        var remove_touches = function (event) {
            if (is_first_selected())
                __current_gesture.select.multiple = false;
    
            if (is_selected())
                return;
    
            window.clearTimeout(on_select_timer);
        };
        var move_touches = function (event) {
            if (is_selected())
                return;
                
            window.clearTimeout(on_select_timer);
        };
    
        __add_touch_events(element, add_touches, remove_touches, move_touches);
    };
    
    var __gestureOnDeselect = function (element, onDeselect) {
        element.gestureEventOnDeselect = onDeselect;
    };
    
    var __gestureOnDragAndDrop = function (element, onDrag, onDrop) {
        var touches = {};
    
        var last_point = null;
    
        var add_touches = function (event) {
            touches = __get_touches(event, element);
            if (touches.length != 1 || __current_gesture.scroll) {
                last_point = null;
                return;
            }
    
            last_point = {
                pageX: Math.ceil(touches[0].pageX),
                pageY: Math.ceil(touches[0].pageY)
            };
        };
        var remove_touches = function (event) {
            if (__current_gesture.scroll)
                return;
    
            touches = __get_touches(event, element);
            if (touches.length != 0 || last_point == null)
                return;
    
            onDrop(element);
        };
        var move_touches = function (event) {
            if (__current_gesture.scroll)
                return;
    
            touches = __get_touches(event, element);
            if (touches.length != 1) {
                last_point = null;
                return;
            }
    
            if (last_point == null)
                return;
    
            var point = {
                pageX: Math.ceil(touches[0].pageX),
                pageY: Math.ceil(touches[0].pageY)
            };
    
            onDrag(element, point.pageX - last_point.pageX, point.pageY - last_point.pageY);
            last_point = point;
        };
    
        __add_touch_events(element, add_touches, remove_touches, move_touches);
    };
    
    var __gestureOnScroll = function (element, onScroll) {
        var touches = {};
    
        var required_touches = 2;
        var last_points = null;
    
        var add_touches = function (event) {
            last_points = null;
            __current_gesture.scroll = false;
        };
        var remove_touches = function (event) {
            last_points = null;
            __current_gesture.scroll = false;
        };
        var move_touches = function () {
            touches = __get_touches(event, element);
            if (touches.length != required_touches) {
                last_points = null;
                __current_gesture.scroll = false;
                return;
            }
    
            __current_gesture.scroll = true;
    
            var points = [];
            for (var i = 0; i < touches.length; i++)
                points.push({
                    pageX: touches[i].pageX,
                    pageY: touches[i].pageY
                });
    
            if (last_points === null) {
                last_points = points;
                return;
            }
    
            var avg_diff_x = 0, avg_diff_y = 0, diff_x, diff_y;
            for (var j = 0; j < required_touches; j++) {
                var x0, x1, y0, y1;
                x0 = last_points[j].pageX;
                x1 = points[j].pageX;
    
                y0 = last_points[j].pageY;
                y1 = points[j].pageY;
    
                avg_diff_x += (x1 - x0);
                avg_diff_y += (y1 - y0);
            }
            avg_diff_x /= required_touches * 1.0;
            avg_diff_y /= required_touches * 1.0;
    
            diff_x = avg_diff_x;
            diff_y = avg_diff_y;
    
            last_points = points;
    
            onScroll(element, diff_x, diff_y);
        };
    
        __add_touch_events(element, add_touches, remove_touches, move_touches);
    };
    
    var __gestureOnDoubleTouch = function (element, onDoubleTouch) {
        var touches = {};
    
        var on_touch_timer = null;
        var touch_counter = 0;
        var clear_counter = function () {
            touch_counter = 0;
            window.clearTimeout(on_touch_timer);
        };
    
        var add_touches = function (event) {
            // No hacer nada.
        };
        var remove_touches = function (event) {
            touches = __get_touches(event, element);
            if (touches.length != 0) {
                clear_counter();
                return;
            }
            
            touch_counter += 1;
            if (touch_counter == 2) {
                onDoubleTouch(element);
                clear_counter();
                return;
            }
    
            window.setTimeout(clear_counter, 250);
        };
        var move_touches = function (event) {
            // No hacer nada.
        };
    
        __add_touch_events(element, add_touches, remove_touches, move_touches);
    };
    
    var __gestureOnRotate = function (element, onRotate) {
        var touches = {};
    
        var timer = null, enabled = false;
        var center_point = null;
        var last_points = null;
    
        var get_points = function() {
            var points = [];
            for (var i = 0; i < touches.length; i++)
                points.push({
                    pageX: touches[i].pageX,
                    pageY: touches[i].pageY
                });
    
            return points;
        };
        var calculate_diff = function () {
            var angle = function (p0, p1, p2) {
                var v1, v2;
        
                // Obtener el vector 1 y el vector 2.
                v1 = {x: p0.pageX - p1.pageX, y: p0.pageY - p1.pageY};
                v2 = {x: p2.pageX - p1.pageX, y: p2.pageY - p1.pageY};
        
                // Obtener la diferencia de ángulos.
                return Math.atan2(v2.y, v2.x) - Math.atan2(v1.y, v1.x);
            };
            
            var a = last_points;
            var b = get_points();
            
            var t = Math.abs(angle(b[0], center_point, b[1]));
            if (3.0 <= t && t <= 3.3)
                return null;
    
            var diff = angle(b[0], center_point, b[1]) - angle(a[0], center_point, a[1]);
            return diff;
        };
        var clear = function() {
            window.clearTimeout(timer);
            
            center_point = null;
            last_points = null;
            enabled = false;
            __current_gesture.rotate = false;
        };
    
        var add_touches = function (event) {
            touches = __get_touches(event, element);
            if (touches.length != 2)
                return clear();
    
            timer = window.setTimeout(function() {
                clear();
            }, 200);
    
            last_points = get_points();
    
            center_point = {
                pageX: (last_points[0].pageX + last_points[1].pageX) / 2.0,
                pageY: (last_points[0].pageY + last_points[1].pageY) / 2.0
            };
        };
        var remove_touches = function (event) {
            clear();
        };
        var move_touches = function (event) {
            if (__current_gesture.scroll || __current_gesture.zoom)
                return clear();
    
            if (last_points == null)
                return clear();
    
            touches = __get_touches(event, element);
            if (touches.length != 2) {
                last_points = null;
                return clear();
            }
    
            var diff_rad = calculate_diff();
            if (!enabled && diff_rad == null)
                return;
    
            if (!enabled) {
                window.clearTimeout(timer);
                enabled = true;
            }
    
            __current_gesture.rotate = true;
            last_points = get_points();
            
            onRotate(element, diff_rad / 1.5);
        };
    
        __add_touch_events(element, add_touches, remove_touches, move_touches);
    };
    
    var __gestureSetBackground = function(element) {
        var touches = {};
    
        var add_touches = function (event) {
            touches = __get_touches(event, element);
            if (touches.length != 1) {
                return;
            }
    
            var selected = document.querySelectorAll('[data-selected="1"]');
            for (var i = 0; i < selected.length; i++) {
                var e = selected[i];
                
                e.dataset['selected'] = '0';
                e.dataset['firstSelected'] = '0';
                e.gestureEventOnDeselect(e);
            }
        };
        var remove_touches = function (event) {
            
        };
        var move_touches = function (event) {
            
        };
    
        __add_touch_events(element, add_touches, remove_touches, move_touches);
    };
    
    this.gestures = {
        onZoom: function(element, onZoom) {
            __gestureOnZoon(element, onZoom);
        },
        onSelect: function(element, onSelect) {
            __gestureOnSelect(element, onSelect);
        },
        onDeselect: function(element, onDeselect) {
            __gestureOnDeselect(element, onDeselect);
        },
        onDragAndDrop: function(element, onDrag, onDrop) {
            __gestureOnDragAndDrop(element, onDrag, onDrop);
        },
        onScroll: function(element, onScroll) {
            __gestureOnScroll(element, onScroll);
        },
        onDoubleTouch: function(element, onDoubleTouch) {
            __gestureOnDoubleTouch(element, onDoubleTouch);
        },
        onRotate: function(element, onRotate) {
            __gestureOnRotate(element, onRotate);
        },
        setBackground: function(element) {
            __gestureSetBackground(element);
        },
    };

}


window.lsf = new LSF();