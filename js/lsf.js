/*
* REGISTRO DE CUSTOM ELEMENTS HTML5 DEL FRAMEWORK
*/
document.registerElement('lsf-button-float');
document.registerElement('lsf-button');
document.registerElement('lsf-button-flat');
document.registerElement('lsf-card');
document.registerElement('lsf-modal');
document.registerElement('lsf-modal-dialog');
document.registerElement('lsf-tabs');
document.registerElement('lsf-tab');
document.registerElement('lsf-switch');
document.registerElement('lsf-notification');
document.registerElement('lsf-workspaces');
document.registerElement('lsf-keyboard');
$("lsf-button").each(function () {
    $(this).addClass("palette-" + $(this).attr("color") + " bg");
});
function toggleModal(id) {
    if ($(id).is(":visible")) {
        $(id).fadeOut(500);
    }
    else {
        $(id).fadeIn(500);
        setTimeout(function () {
            $(id + " > lsf-modal-dialog").css("height", $(id + " > lsf-modal-dialog").outerHeight() + "px");
            $(id + " > lsf-modal-dialog").css("bottom", "0");
        }, 500);
    }
}
/// <reference path="./lsfInterfaces.ts" />
/// <reference path="./lsfUIRegistrations.ts" />
/// <reference path="../typings/async/async.d.ts"/>
/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/RTCPeerConnection/RTCPeerConnection.d.ts" />
/**
 * Contiene toda la funcionalidad de la librería annyang! para el reconocimiento de voz.
 * @lends {object}
 */
var annyang;
/**
 * Clase Principal del Framework, contiene 4 módulos desarrollados:
 * <ol>
 * <li>Connectivity: Brinda la facilidad de poder conectarse vía WebRTC o Websockets con cualquier servidor que siga dichos estándares</li>
 * <li>Gestures: Brinda la capacidad de poder establecer metodos propios para la gestión de los gestos diseñados para Large Screen Surfaces </li>
 * <li>SpeechRecognition: Brinda la capacidad de poder usar la voz como comandos para poder aumentar la productividad y realizar tareas de manera sencilla </li>
 * <li>Workspaces: Brinda la logistica para poder brindar la capacidad de uso del Large Screen Surface a más de una persona </li>
 * </ol>
 * @namespace LSF
 * @class LSF
 */
var LSF = (function () {
    function LSF() {
        var _this = this;
        /**
         * Información del dispositivo actual.
         *
         * @type {largeScreenFramework._lsf_plot_device}
         */
        this.device = {
            uniqueId: -1,
            friendlyName: "Client Device",
            ipAddress: "",
            platform: navigator.userAgent,
            longitude: "",
            latitude: ""
        };
        /**
         * Variable que permite activar o desactivar la salida de las trazas por consola.
         * @memberof LSF
         * @prop {true | false } debug  <code>true</code> Activa por consola los logs y outputs de las funciones (recomendado para entornos de desarrollo),<br/><code>false</code> Desactiva todos los logs del framework (recomendado para entornos de produccción)
         */
        this.debug = false;
        /**
         * Funciones para poder mostrar información en la consola.
         *
         * @type {largeScreenFramework._lsf_debug}
         */
        this.log = {
            trace: function () {
                var parameters = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    parameters[_i - 0] = arguments[_i];
                }
                if (_this.debug) {
                    console.log.apply(console, parameters);
                }
            },
            info: function () {
                var parameters = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    parameters[_i - 0] = arguments[_i];
                }
                if (_this.debug) {
                    console.info.apply(console, parameters);
                }
            },
            warn: function () {
                var parameters = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    parameters[_i - 0] = arguments[_i];
                }
                if (_this.debug) {
                    console.warn.apply(console, parameters);
                }
            },
            error: function () {
                var parameters = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    parameters[_i - 0] = arguments[_i];
                }
                if (_this.debug) {
                    console.error.apply(console, parameters);
                }
            }
        };
        /**
         * Lista de comandos que pueden ser envíados vía WebSocket para la comunicación bidireccional entre el frontend y el backend
         * @memberof LSF
         * @prop {string} message - Comando de tipo mensaje (usado para enviar cadenas de texto plano)
         * @prop {string} error - Comando de tipo error (usado para enviar las colas de trazabilidad)
         * @prop {string} event - Comando de tipo evento (usado para enviar funciones personalizadas al dispararse una eventualidad)
         * @prop {object} webSocket - Listado de comandos disponibles para Websockets
         *  @prop {object} webSocket.discoveryServers discoveryServers  Trama que es invocada cuando se requiere conectarse y obtener información de toda la red en busca de servidores
         *      @prop {string} webSocket.discoveryServers.request Petición que envia el frontend
         *      @prop {string} webSocket.discoveryServers.response Petición que recibe el frontend
         *  @prop {object} webSocket.updateDevicesInfo Trama que se envía para poder actualizar la lista de nodos conectados al servidor
         *      @prop {string} webSocket.updateDevicesInfo.request Petición que envia el frontend
         *      @prop {string} webSocket.updateDevicesInfo.response Petición que recibe el frontend
         *  @prop {object} webSocket.getUniqueId Trama que es invocada cuando se solicita y recive el id Unico que permite al servidor reconocer a los nodos conectados
         *      @prop {string} webSocket.getUniqueId.request  Petición que envia el frontend
         *      @prop {string} webSocket.getUniqueId.response Petición que recibe el frontend
         */
        this.commandsName = {
            message: "message",
            error: "error",
            event: "event",
            webSocket: {
                discoveryServers: {
                    request: "ws_req_discoveryServers",
                    response: "ws_res_discoveryServers"
                },
                updateDevicesInfo: {
                    request: "ws_req_updateDevicesInfo",
                    response: "ws_res_updateDevicesInfo"
                },
                getUniqueId: {
                    request: "ws_req_getUniqueId",
                    response: "ws_res_getUniqueId"
                },
                sendDevice: "ws_sendDevice"
            }
        };
        /**
         * Funcionalidad interna para controlar una conexión WebSocket.
         *
         * @type {largeScreenFramework._lsf_webSocketModule}
         */
        this.webSocket = {
            connection: null,
            isActive: function () {
                return _this.webSocket.connection != null
                    && _this.webSocket.connection.established
                    && _this.webSocket.tools.localIPReady;
            },
            connectionConfig: {
                host: "localhost",
                usingSecure: false,
                protocol: {
                    secure: "wss://",
                    insecure: "ws://"
                },
                port: 8080,
                keepAlive: 5
            },
            establishConnection: function (config, onSuccess, onError, onClose) {
                var address = "";
                if (config.usingSecure) {
                    address = config.protocol.secure + config.host + ":" + config.port;
                }
                else {
                    address = config.protocol.insecure + config.host + ":" + config.port;
                }
                try {
                    _this.log.info("Intentando establecer conexión con el servidor...");
                    _this.webSocket.connection = new WebSocket(address);
                }
                catch (e) {
                    _this.log.error("No se puedo establecer la conexion con el servidor.");
                    _this.log.trace(e);
                    if (typeof onError == 'function') {
                        onError(e);
                    }
                }
                if (_this.webSocket.connection !== null) {
                    _this.webSocket.connection.established = false;
                    _this.webSocket.connection.onopen = function (event) {
                        _this.log.trace(event);
                        _this.log.info("Se ha establecido la conexion con el servidor.");
                        _this.webSocket.openConnection(onSuccess, onError, onClose);
                        if (config.keepAlive > 0) {
                            var interval = setInterval(function () {
                                if (!_this.webSocket.isActive()) {
                                    clearInterval(interval);
                                }
                                else {
                                    var keepAlive = {
                                        command: "keepAlive"
                                    };
                                    _this.webSocket.connection.send(JSON.stringify(keepAlive));
                                }
                            }, config.keepAlive * 1000);
                        }
                    };
                    _this.webSocket.connection.onerror = function (e) {
                        _this.log.error(e);
                        if (typeof onError == 'function') {
                            onError(e);
                        }
                    };
                    _this.webSocket.connection.onclose = function (event) {
                        _this.log.info("Se ha cerrado la conexion con el servidor.");
                        _this.log.trace(event);
                        _this.webSocket.connection.established = false;
                        if (typeof onClose == 'function') {
                            onClose(event);
                        }
                    };
                }
            },
            openConnection: function (onSuccess, onError, onClose) {
                _this.webSocket.connection.established = true;
                _this.webSocket.connection.onerror = function (err) {
                    _this.log.error(err);
                    if (typeof onError == 'function') {
                        onError(err);
                    }
                };
                _this.webSocket.connection.onclose = function (event) {
                    _this.log.info("Se ha cerrado la conexion con el servidor");
                    _this.log.trace(event);
                    _this.webSocket.connection.established = false;
                    if (typeof onClose == 'function') {
                        onClose(event);
                    }
                };
                _this.webSocket.connection.onmessage = function (serializedData) {
                    _this.log.trace(["Recibido:", serializedData.data]);
                    var plot = JSON.parse(serializedData.data);
                    if (plot != null && plot.command != null) {
                        if (plot.command in _this.connectivity.userCallbacks) {
                            _this.connectivity.userCallbacks[plot.command](plot);
                        }
                        switch (plot.command) {
                            case _this.commandsName.message: {
                                break;
                            }
                            case _this.commandsName.event: {
                                break;
                            }
                            case _this.commandsName.error: {
                                break;
                            }
                            case _this.commandsName.webSocket.discoveryServers.response: {
                                break;
                            }
                            case _this.commandsName.webSocket.getUniqueId.response: {
                                _this.device.uniqueId = plot.customData;
                                _this.log.info("El servidor ha registrado el dispositivo en la lista, el id es: " + plot.customData + ".");
                                break;
                            }
                            case _this.commandsName.webSocket.updateDevicesInfo.response: {
                                break;
                            }
                        }
                    }
                    else {
                        _this.log.error("El comando no es compatible con la apliacion.");
                        if (typeof onError == 'function') {
                            onError("Comando no compatible.");
                        }
                    }
                };
                if (typeof onSuccess == 'function') {
                    onSuccess();
                }
            },
            tools: {
                localIPReady: false,
                avaliableServers: [],
                findLocalIP: function (success, error) {
                    var ip_dups = {};
                    var RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
                    // var useWebKit = !!window.webkitRTCPeerConnection;
                    var mediaConstraints = {
                        optional: [{
                                RtpDataChannels: true
                            }]
                    };
                    var servers = {
                        iceServers: [{
                                urls: "stun:stun.services.mozilla.com"
                            }]
                    };
                    var pc = new RTCPeerConnection(servers, mediaConstraints);
                    var handleCandidate = function (candidate) {
                        var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/;
                        if (ip_regex.exec(candidate) !== null) {
                            var ip_addr = ip_regex.exec(candidate)[1];
                            if (ip_dups[ip_addr] === undefined) {
                                _this.log.trace("IP Local encontrada: " + ip_addr);
                                _this.webSocket.tools.localIPReady = true;
                                if (typeof success == 'function') {
                                    success(ip_addr);
                                }
                            }
                            ip_dups[ip_addr] = true;
                        }
                    };
                    pc.onicecandidate = function (ice) {
                        if (ice.candidate) {
                            handleCandidate(ice.candidate.candidate);
                        }
                    };
                    pc.createDataChannel("");
                    pc.createOffer(function (result) {
                        pc.setLocalDescription(result, function () {
                        }, function () {
                        });
                    }, function () {
                    });
                    setTimeout(function () {
                        if (pc.setLocalDescription.sdp) {
                            var lines = pc.setLocalDescription.sdp.split('\n');
                            lines.forEach(function (line) {
                                if (line.indexOf('a=candidate:') === 0) {
                                    handleCandidate(line);
                                }
                            });
                        }
                    }, 1000);
                },
                discoveryServers: function (config, onDiscovery) {
                    _this.webSocket.tools.avaliableServers = [];
                    var serversPool = [];
                    var splittedIP = config.host.split(".", 3);
                    var mask = splittedIP[0] + "." + splittedIP[1] + "." + splittedIP[2];
                    var that = _this;
                    for (var i = 2; i < 255; i++) {
                        try {
                            var address = null;
                            if (config.usingSecure) {
                                address = config.protocol.secure + mask + "." + i + ":" + config.port;
                            }
                            else {
                                address = config.protocol.insecure + mask + "." + i + ":" + config.port;
                            }
                            var ws = new WebSocket(address);
                            serversPool.push(ws);
                            serversPool[i - 2].onopen = function () {
                                that.log.info("Se ha encontrado un nuevo servidor en " + this.url);
                                that.log.trace("Enviando peticion de descubrimiento...");
                                var discoveryPlot = JSON.stringify({
                                    command: that.commandsName.webSocket.discoveryServers.request
                                });
                                this.send(discoveryPlot);
                                that.log.info("Enviado: " + discoveryPlot);
                            };
                            serversPool[i - 2].onmessage = function (serializedData) {
                                var decodePlot = JSON.parse(serializedData.data);
                                that.log.info("Recibido: " + serializedData.data);
                                if (decodePlot.command == that.commandsName.webSocket.discoveryServers.response) {
                                    var url = this.url;
                                    var serverCandidate = {
                                        url: url,
                                        status: decodePlot.customData
                                    };
                                    that.webSocket.tools.avaliableServers.push(serverCandidate);
                                    if (typeof onDiscovery == 'function') {
                                        onDiscovery(decodePlot);
                                    }
                                }
                            };
                        }
                        catch (e) {
                            _this.log.error(e);
                        }
                    }
                }
            }
        };
        // private webRTC: largeScreenFramework._lsf_webRTCModule = {};
        /**
         *
         * @type {_lsf_gesturesHelper}
         */
        this.gesturesHelper = {
            getTouches: (function (event, element) {
                var touches = [];
                var event_touches = element ? event.targetTouches : event.touches;
                for (var i = 0; i < event_touches.length; i++) {
                    if (element && element !== event.target)
                        continue;
                    touches[touches.length] = event_touches[i];
                }
                return touches;
            }),
            addTouchEvents: (function (element, add_touches, remove_touches, move_touches) {
                // Cuando uno o más toques aparecen en la pantalla.
                element.addEventListener('touchstart', add_touches, false);
                element.addEventListener('touchenter', add_touches, false);
                // Cuando uno o más toques se mueven.
                element.addEventListener('touchmove', move_touches, false);
                // Cuando uno o más toques desaparecen de la pantalla.
                element.addEventListener('touchcancel', remove_touches, false);
                element.addEventListener('touchend', remove_touches, false);
                element.addEventListener('touchleave', remove_touches, false);
            }),
            currentGesture: {
                scroll: false,
                rotate: false,
                zoom: false,
                select: {
                    multiple: false
                }
            }
        };
        this.internalWorkspaces = {
            initializeWorkspace: function (element) {
                _this.virtualKeyBoard.create(element);
                return true;
            },
            setCallbackByKeyboard: function (workspace, callback) {
                _this.internalWorkspaces.callbacksByKeyboard[workspace] = callback;
            },
            callbacksByKeyboard: {},
            registerUser: function (element, user) {
                return true;
            },
            rotateWorkspace: function (element, angle) {
                return true;
            },
            setWorkspaceColor: function (element, color) {
                return true;
            },
            sharedWorkspaces: false
        };
        this.virtualKeyBoard = {
            create: function (workspace) {
                var self = _this;
                var keys;
                keys = [];
                keys[0] = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'];
                keys[1] = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ñ'];
                keys[2] = ['Shift', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 127];
                keys[3] = ['?1', 'Espacio', 13];
                var keysalt;
                keysalt = [];
                keysalt[0] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
                keysalt[1] = ['@', '#', '$', '_', '&', '-', '+', '(', ')', '/'];
                keysalt[2] = [',', '.', '*', '"', "'", ':', ';', '!', '?', 127];
                keysalt[3] = ['?1', 'Espacio', 13];
                var keyboard = "<lsf-keyboard data-caps='false' data-workspace='" + $(workspace).attr('id') + "'><div class='lsf_normal_layout'>";
                _this.virtualKeyBoard.caps[workspace] = false;
                for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                    var keyrow = keys_1[_i];
                    for (var _a = 0, keyrow_1 = keyrow; _a < keyrow_1.length; _a++) {
                        var keyitem = keyrow_1[_a];
                        if (typeof keyitem === "number") {
                            if (keyitem == 127)
                                keyboard += "<span class='key-button' data-key='" + keyitem.toString() + "' style='width:20%;'><i class='material-icons'>backspace</i></span>";
                            else if (keyitem == 13)
                                keyboard += "<span class='key-button' data-key='" + keyitem.toString() + "' style='width:20%;'><i class='material-icons'>send</i></span>";
                            else
                                keyboard += "<span class='key-button' data-key='" + keyitem.toString() + "' style=''>" + keyitem.toString() + "</span>";
                        }
                        else if (typeof keyitem === "string") {
                            if (keyitem == "Shift")
                                keyboard += "<span class='key-button' data-key='" + keyitem + "' style=''><i class='material-icons'>keyboard_capslock</i></span>";
                            else if (keyitem == "Espacio")
                                keyboard += "<span class='key-button' data-key='" + keyitem + "' style='width: 50%;'><i class='material-icons'>space_bar</i></span>";
                            else
                                keyboard += "<span class='key-button' data-key='" + keyitem + "' style=''>" + keyitem + "</span>";
                        }
                    }
                    keyboard += "<br/>";
                }
                keyboard += "</div><div class='lsf_special_layout'>";
                for (var _b = 0, keysalt_1 = keysalt; _b < keysalt_1.length; _b++) {
                    var keyrow = keysalt_1[_b];
                    for (var _c = 0, keyrow_2 = keyrow; _c < keyrow_2.length; _c++) {
                        var keyitem = keyrow_2[_c];
                        if (typeof keyitem === "number") {
                            if (keyitem == 127)
                                keyboard += "<span class='key-button' data-key='" + keyitem.toString() + "' style=''><i class='material-icons'>backspace</i></span>";
                            else if (keyitem == 13)
                                keyboard += "<span class='key-button' data-key='" + keyitem.toString() + "' style='width:20%;'><i class='material-icons'>send</i></span>";
                            else
                                keyboard += "<span class='key-button' data-key='" + keyitem.toString() + "' style=''>" + keyitem.toString() + "</span>";
                        }
                        else if (typeof keyitem === "string") {
                            if (keyitem == "Shift")
                                keyboard += "<span class='key-button' data-key='" + keyitem + "' style=''><i class='material-icons'>keyboard_capslock</i></span>";
                            else if (keyitem == "Espacio")
                                keyboard += "<span class='key-button' data-key='" + keyitem + "' style='width: 50%;'><i class='material-icons'>space_bar</i></span>";
                            else
                                keyboard += "<span class='key-button' data-key='" + keyitem + "' style=''>" + keyitem + "</span>";
                        }
                    }
                    keyboard += "<br/>";
                }
                keyboard += "</div><div data-event='close' style='position: absolute; text-align: center; background-color: red; color: white; width: 50px; height: 25px; top: -25px; right: 0; border-top-left-radius: 2px; border-top-right-radius: 2px; font-weight: bold; font-size: 18px;'>&times;</div></lsf-workspace>";
                $(workspace).append(keyboard);
                $(workspace).find("lsf-keyboard").find("[data-event='close']").on("click", function () {
                    self.workspaces.moveKeyBoard(workspace, -1000, -1000);
                });
                $(workspace).find("lsf-keyboard").find(".key-button").each(function (index, button) {
                    $(button).on("click", function () {
                        if ($(this).data("key") == "Shift") {
                            self.virtualKeyBoard.toggleCases($(workspace).attr("id"));
                        }
                        else if (($(this).data("key") == "?1")) {
                            if (self.virtualKeyBoard.caps["#" + $(workspace).attr("id")]) {
                                self.virtualKeyBoard.toggleCases($(workspace).attr("id"));
                            }
                            self.virtualKeyBoard.toggleView($(workspace).attr("id"));
                        }
                        else if (($(this).data("key") == "Espacio")) {
                            self.internalWorkspaces.callbacksByKeyboard["#" + $(workspace).attr("id")](" ");
                        }
                        else if (($(this).data("key") == 13)) {
                            self.internalWorkspaces.callbacksByKeyboard["#" + $(workspace).attr("id")]("\n");
                        }
                        else if (($(this).data("key") == 127)) {
                            self.internalWorkspaces.callbacksByKeyboard["#" + $(workspace).attr("id")]("\b");
                        }
                        else {
                            if (self.virtualKeyBoard.caps["#" + $(workspace).attr("id")])
                                self.internalWorkspaces.callbacksByKeyboard["#" + $(workspace).attr("id")]($(this).data("key").toUpperCase());
                            else
                                self.internalWorkspaces.callbacksByKeyboard["#" + $(workspace).attr("id")]($(this).data("key"));
                        }
                    });
                });
                return true;
            },
            delete: function (workspace) {
                return true;
            },
            toggleView: function (workspace) {
                console.log(workspace);
                var keyboard = $("#" + workspace).find("lsf-keyboard");
                if ($(keyboard).find(".lsf_normal_layout").is(":visible")) {
                    $(keyboard).find(".lsf_normal_layout").fadeOut(0);
                    $(keyboard).find(".lsf_special_layout").fadeIn(0).css("display", "flex");
                }
                else {
                    $(keyboard).find(".lsf_normal_layout").fadeIn(0).css("display", "flex");
                    ;
                    $(keyboard).find(".lsf_special_layout").fadeOut(0);
                }
            },
            toggleCases: function (workspace) {
                if ($("#" + workspace).find("lsf-keyboard").attr("data-caps") == "false") {
                    $("#" + workspace).find(".lsf_normal_layout > span").each(function (index, elem) { $(elem).css("text-transform", "uppercase"); });
                    $("#" + workspace).find("lsf-keyboard").attr("data-caps", "true");
                    _this.virtualKeyBoard.caps["#" + workspace] = true;
                }
                else {
                    $("#" + workspace).find(".lsf_normal_layout > span").each(function (index, elem) { $(elem).css("text-transform", "lowercase"); });
                    $("#" + workspace).find("lsf-keyboard").attr("data-caps", "false");
                    _this.virtualKeyBoard.caps["#" + workspace] = false;
                }
            },
            caps: {}
        };
        /**
         * Función que se utiliza para inicializar todo el framework, en ella se disparan los eventos necesarios para que el framework quede completamente operativo:
         * <ol>
         * <li>Inicializa la IP Local (Requisito Básico para poder usar la función de discoveryServers)</li>
         * <li>Inicializa la pre-carga de la librería de reconocimiento de voz Annyang!</li>
         * <li>Inicializa la carga de los workspaces asignandoles automaticamente su propio Teclado Virtual </li>
         * </ol>
         * @memberof LSF
         * @function ready
         * @param onSuccess {function} función que se ejecutará cuando la carga inicial del framework haya sido exitosa
         * @param onError {function} funcion que se ejecutará cuando la carga inicial del framework haya tenido un error
         * @example
         * var LSF.ready(function(){
         *  console.log("El framework ha cargado de manera exitosa");
         * },
         * function(err){
         *  console.log("El framework ha tenido inconvenientes para cargar completamente", + err);
         * });
         */
        this.ready = function (onSuccess, onError) {
            var self = _this;
            try {
                _this.log.info("Inicializando la IP");
                _this.webSocket.tools.findLocalIP(function (ip) {
                    if (ip.match(/^(192\.168\.|169\.254\.|10\.|172\.(1[6-9]|2\d|3[01]))/)) {
                        _this.log.info(["ip:", ip]);
                        _this.device.ipAddress = ip;
                        _this.webSocket.tools.localIPReady = true;
                    }
                }, function () {
                    // TODO: Revisar cuando ocurra un error.
                });
                _this.log.info("Inicializando el reconocimiento de voz");
            }
            catch (e) {
                onError(e);
            }
            _this.log.info("Inicializando los workspaces");
            $("body").find("lsf-workspace").each(function (index, workspace) {
                self.internalWorkspaces.initializeWorkspace(workspace);
            });
            onSuccess();
        };
        /**
         * Módulo que permite manipular espacios de trabajos para que diferentes personas puedan manipular la aplicacion al mismo tiempo
         * @memberof LSF
         * @namespace {lsf_workspaces} workspaces
    
         */
        this.workspaces = {
            /**
             * Función que permite realizar un enlazamiento entre una acción y la tecla que se presiona en el teclado de dicho workspace
             * @function onKeyBoardPress
             * @memberof LSF.workspaces
             * @param {string} idWorkspace Identificador del Workspace a enlazar
             * @param {function} callback Función que devuelve como parametro la tecla que se ha presionado
             * @example
             * //Ejemplo de onKeyBoardPress
             * lsf.workspaces.onKeyBoardPress("#workspace1", function(tecla){
             *   //Accion a realizar con la tecla recibida por el parametro del callback
             * })
             */
            onKeyBoardPress: function (idWorkspace, callback) {
                _this.internalWorkspaces.setCallbackByKeyboard(idWorkspace, callback);
            },
            /**
             * Función que permite mover el teclado del Workspace a una determinada posición x,y
             * @function moveKeyBoard
             * @memberof LSF.workspaces
             * @param {string} idWorkspace Identificador del Workspace padre del teclado
             * @param {number} posx Cantidad relativa al workspace que se va a trasladar horizontalmente
             * @param {number} posy Cantidad relativa al workspace que se va a trasladar verticalmente
             * @example
             * //Ejemplo de moveKeyBoard
             * //En este ejemplo se mueve el teclado 30px a la derecha y 40px
             * //hacia abajo relativo al workspace
             * lsf.workspaces.moveKeyBoard("#workspace1", 30, 40);
             */
            moveKeyBoard: function (idWorkspace, posx, posy) {
                $(idWorkspace).find("lsf-keyboard").css({ "top": posy + "px", "left": posx + "px" });
            }
        };
        /**
         * Funcionalidad pública para controlar la librería annyang!.
         * @memberof LSF
         * @namespace {_lsf_speechRecognitionModules} speechRecognition
    
         */
        this.speechRecognition = {
            /**
             * Función que permite configurar el lenguaje del reconocimiento de voz
             * @function setLanguage
             * @memberof LSF.speechRecognition
             * @param {string} language valor que se agrega para poder establecer un idioma de reconocimiento, [Haga click aquí]{@link https://github.com/TalAter/annyang/blob/master/docs/FAQ.md#what-languages-are-supported} para ver todos los idiomas disponibles
             * @example
             *  LSF.speechRecognition.setLanguage("es-ES");
             */
            setLanguage: function (language) {
                annyang.setLanguage(language);
            },
            /**
             * Permite añadir un callback a determinado evento proporcionado por la librería Annyang!
             * @function addCallback
             * @memberof LSF.speechRecognition
             * @param {Event} index Evento que va a ser disparado, [Haga click aquí]{@link https://github.com/TalAter/annyang/blob/master/docs/README.md#addcallbacktype-callback-context} para poder visualizar los eventos disponibles a usar
             * @param {function} callback Función que se ejecutará cuando suceda el evento seleccionado
             * @example
             * LSF.speechRecognition.addCallback("resultNoMatch",function(){
             *  console.log("Lo sentimos no hemos podido interpretar lo que quizo decir, por favor intentelo de nuevo");
             * })
             */
            addCallback: function (index, callback) {
                annyang.addCallback(index, callback);
            },
            /**
             * Permite añadir un comando que sirve de patrón para identificar bajo que condiciones se va a ejecutar un determinado callback
             * @function addCommand
             * @memberof LSF.speechRecognition
             * @param {string} command Texto que sirve de patrón a reconocer para poder ejecutar una función estas pueden ser: <br/> <ul><li>"un texto simple"</li><li>"Un texto con mas parametros *adelante" (donde adelante se reemplazará con todo lo que se escuche despues)</li><li>"Texto con una :variable" (donde :variable será reemplazado con una sola palabra)</li><li>"O un ejemplo (pequeño) de prueba" (donde (pequeño) puede ser omitido si es que no se dice)</ul>
             * @param {function} callback Función que se llamará al encontrar el patrón dado en el parámetro 'command'
             * @example
             * LSF.speechRecognition.addCommand('Mostrar cuadrados :color',function(color){
             *      //Funcion para mostrar solo los cuadrados que cumplan con :color
             * })
             */
            addCommand: function (command, callback) {
                _this.speechRecognition.commands[command] = callback;
            },
            commands: {},
            /**
             * Funcion que inicia la solicitud del micrófono y comienza a ejecutarse el reconocimiento de voz (Se requiere que previamente se hayan configurado los comandos)
             * @funcion start
             * @memberof LSF.speechRecognition
             * @example
             * LSF.speechRecognition.start();
             */
            start: function () {
                annyang.addCommands(_this.speechRecognition.commands);
                annyang.start();
            }
        };
        /**
         * Módulo que permite gestionar los gestos táctiles
         * @memberof LSF
         * @namespace {_lsf_gesturesModule} gestures
         */
        this.gestures = {
            /**
             * Funcion para habilitar el gesto onZoom
             * @memberof LSF.gestures
             * @function onZoom
             * @param {Element} element Elemento del DOM que va a ser afectado
             * @param {callback} onZoom Funcion que se ejecuta al realizar el gesto devolviendo el elemento y la diferencia entre los 2 puntos de contacto del zoom
             * @example
             * var elemento = document.getElementById("Prueba");
             * LSF.gestures.onZoom(elemento,function(el, diff){
             *     el.innerHTML = "la separación entre los dos puntos de zoom es " + diff + " px de distancia";
             * })
             */
            onZoom: (function (element, onZoom) {
                var touches = [];
                var timer = null, enabled = false;
                var center_point = null;
                var last_points = null;
                var get_points = function () {
                    var points = [];
                    for (var i = 0; i < touches.length; i++)
                        points.push({
                            pageX: touches[i].pageX,
                            pageY: touches[i].pageY
                        });
                    return points;
                };
                var calculate_diff = function () {
                    var distance = function (a, b) {
                        // Distance = √[(x1 − x0) ^ 2 + (y1 − y0) ^ 2]
                        var distance = 0.0;
                        distance += Math.pow(b.pageX - a.pageX, 2);
                        distance += Math.pow(b.pageY - a.pageX, 2);
                        distance = Math.sqrt(distance);
                        return distance;
                    };
                    var a = last_points;
                    var b = get_points();
                    var d1 = distance(a[0], center_point) + distance(a[1], center_point);
                    var d2 = distance(b[0], center_point) + distance(b[1], center_point);
                    return d2 - d1;
                };
                var clear = function () {
                    window.clearTimeout(timer);
                    center_point = null;
                    last_points = null;
                    enabled = false;
                    _this.gesturesHelper.currentGesture.zoom = false;
                };
                var add_touches = function (event) {
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 2)
                        return clear();
                    timer = window.setTimeout(function () {
                        enabled = true;
                    }, 200);
                    last_points = get_points();
                    center_point = {
                        pageX: (last_points[0].pageX + last_points[1].pageX) / 2.0,
                        pageY: (last_points[0].pageY + last_points[1].pageY) / 2.0
                    };
                };
                var remove_touches = function (event) {
                    // Reiniciar todos los valores.
                    clear();
                };
                var move_touches = function (event) {
                    if (_this.gesturesHelper.currentGesture.scroll || _this.gesturesHelper.currentGesture.rotate) {
                        clear();
                        return;
                    }
                    if (last_points == null) {
                        clear();
                        return;
                    }
                    if (!enabled)
                        return;
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 2) {
                        clear();
                        return;
                    }
                    _this.gesturesHelper.currentGesture.zoom = true;
                    var diff_s = calculate_diff();
                    last_points = get_points();
                    onZoom(element, diff_s / 100.0);
                };
                _this.gesturesHelper.addTouchEvents(element, add_touches, remove_touches, move_touches);
            }),
            /**
             * Función que permite seleccionar un elemento tras haberlo presionado
             * @memberof LSF.gestures
             * @function onSelect
             * @param {Element} element El elemento a ejecutarse el gesto
             * @param {callback} onSelect callback Funcion que se ejecuta al realizar el gesto devolviendo el elemento
             * @example
             * var elemento = document.getElementById("prueba");
             *  LSF.gestures.onSelect(elemento, function(el){
             *  el.innerHTML = "se ha seleccionado el elemento";
             * });
             */
            onSelect: (function (element, onSelect) {
                var touches = [];
                var html_element = element;
                var on_select_timer = null;
                var is_selected = function () {
                    return html_element.dataset.hasOwnProperty('selected') && html_element.dataset['selected'] == '1';
                };
                var is_first_selected = function () {
                    return html_element.dataset.hasOwnProperty('firstSelected') && html_element.dataset['firstSelected'] == '1';
                };
                var is_multi_select = function () {
                    var selected = document.querySelectorAll('[data-selected="1"]');
                    return selected.length >= 1;
                };
                var add_touches = function (event) {
                    if (is_selected() && !_this.gesturesHelper.currentGesture.select.multiple)
                        return;
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 1) {
                        window.clearTimeout(on_select_timer);
                        return;
                    }
                    if (is_multi_select() && _this.gesturesHelper.currentGesture.select.multiple) {
                        if (is_selected()) {
                            html_element.dataset['selected'] = '0';
                            html_element.dataset['firstSelected'] = '0';
                            if (typeof html_element['gestureEventOnDeselect'] == 'function') {
                                html_element['gestureEventOnDeselect'](element);
                            }
                            return;
                        }
                        html_element.dataset['selected'] = '1';
                        onSelect(element);
                        return;
                    }
                    on_select_timer = window.setTimeout(function () {
                        // Deselect all on first select.
                        var selected = document.querySelectorAll('[data-selected="1"]');
                        for (var i = 0; i < selected.length; i++) {
                            var e = selected[i];
                            e.dataset['selected'] = '0';
                            e.dataset['firstSelected'] = '0';
                            if (typeof e['gestureEventOnDeselect'] == 'function') {
                                e['gestureEventOnDeselect'](e);
                            }
                        }
                        _this.gesturesHelper.currentGesture.select.multiple = true;
                        html_element.dataset['selected'] = '1';
                        html_element.dataset['firstSelected'] = '1';
                        onSelect(element);
                    }, 500);
                };
                var remove_touches = function (event) {
                    if (is_first_selected())
                        _this.gesturesHelper.currentGesture.select.multiple = false;
                    if (is_selected())
                        return;
                    window.clearTimeout(on_select_timer);
                };
                var move_touches = function (event) {
                    if (is_selected())
                        return;
                    window.clearTimeout(on_select_timer);
                };
                _this.gesturesHelper.addTouchEvents(element, add_touches, remove_touches, move_touches);
            }),
            /**
             * Funcion que permite deseleccionar un elemento previamente deseleccionado
             * @memberof LSF.gestures
             * @function onDeselect
             * @param {Element} elemento Elemento a ejecutarse el gesto
             * @param {callback} onDeselect Funcion que se ejecuta al realizar el gesto devolviendo el elemento
             * @example
             * var elemento = document.getElementById("prueba");
             * LSF.gestures.onDeselect(elemento,function(el){
             *  el.innerHTML = "Se ha deseleccionado el elemento!";
             * })
             */
            onDeselect: (function (element, onDeselect) {
                element['gestureEventOnDeselect'] = onDeselect;
            }),
            onDragAndDrop: (function (element, onDrag, onDrop) {
                var touches = [];
                var last_point = null;
                var add_touches = function (event) {
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 1 || _this.gesturesHelper.currentGesture.scroll) {
                        last_point = null;
                        return;
                    }
                    last_point = {
                        pageX: Math.ceil(touches[0].pageX),
                        pageY: Math.ceil(touches[0].pageY)
                    };
                };
                var remove_touches = function (event) {
                    if (_this.gesturesHelper.currentGesture.scroll)
                        return;
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 0 || last_point == null)
                        return;
                    onDrop(element);
                };
                var move_touches = function (event) {
                    if (_this.gesturesHelper.currentGesture.scroll)
                        return;
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 1) {
                        last_point = null;
                        return;
                    }
                    if (last_point == null)
                        return;
                    var point = {
                        pageX: Math.ceil(touches[0].pageX),
                        pageY: Math.ceil(touches[0].pageY)
                    };
                    onDrag(element, point.pageX - last_point.pageX, point.pageY - last_point.pageY);
                    last_point = point;
                };
                _this.gesturesHelper.addTouchEvents(element, add_touches, remove_touches, move_touches);
            }),
            /**
             * Funcion que permite realizar el gesto de scroll sobre un elemento en específico
             * @memberof LSF.gestures
             * @function onScroll
             * @param {Element} elemento Elemento a ejecutarse el gesto
             * @param {callback} onScroll Funcion que se ejecuta al realizar el gesto devolviendo el elemento y las diferencias desde la posicion actual, tanto de X como de Y
             * @example
             * var elemento = document.getElementById("prueba");
             * LSF.gestures.onDeselect(elemento,function(el,diffX,diffY){
             *  el.innerHTML = "Se ha deseleccionado el elemento!, el desplazamiento en X ha sido "+ diffX +" y el desplazamiento en Y ha sido " + diffY;
             * })
             */
            onScroll: (function (element, onScroll) {
                var touches = [];
                var required_touches = 2;
                var last_points = null;
                var add_touches = function (event) {
                    last_points = null;
                    _this.gesturesHelper.currentGesture.scroll = false;
                };
                var remove_touches = function (event) {
                    last_points = null;
                    _this.gesturesHelper.currentGesture.scroll = false;
                };
                var move_touches = function (event) {
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != required_touches) {
                        last_points = null;
                        _this.gesturesHelper.currentGesture.scroll = false;
                        return;
                    }
                    _this.gesturesHelper.currentGesture.scroll = true;
                    var points = [];
                    for (var i = 0; i < touches.length; i++)
                        points.push({
                            pageX: touches[i].pageX,
                            pageY: touches[i].pageY
                        });
                    if (last_points === null) {
                        last_points = points;
                        return;
                    }
                    var avg_diff_x = 0, avg_diff_y = 0, diff_x, diff_y;
                    for (var j = 0; j < required_touches; j++) {
                        var x0, x1, y0, y1;
                        x0 = last_points[j].pageX;
                        x1 = points[j].pageX;
                        y0 = last_points[j].pageY;
                        y1 = points[j].pageY;
                        avg_diff_x += (x1 - x0);
                        avg_diff_y += (y1 - y0);
                    }
                    avg_diff_x /= required_touches * 1.0;
                    avg_diff_y /= required_touches * 1.0;
                    diff_x = avg_diff_x;
                    diff_y = avg_diff_y;
                    last_points = points;
                    onScroll(element, diff_x, diff_y);
                };
                _this.gesturesHelper.addTouchEvents(element, add_touches, remove_touches, move_touches);
            }),
            /**
             * Funcion DoubleTouch
             * @memberof LSF.gestures
             * @function onDoubleTouch
             * @param {Element} elemento Elemento a ejecutarse el gesto
             * @param {callback} onScroll Funcion que se ejecuta al realizar el gesto devolviendo el elemento
             * @example
             * var elemento = document.getElementById("prueba");
             * LSF.gestures.onDoubleTouch(elemento,function(el){
             *  el.innerHTML = "Se ha realizado un dobletouch sobre el elemento";
             * })
             */
            onDoubleTouch: (function (element, onDoubleTouch) {
                var touches = [];
                var on_touch_timer = null;
                var touch_counter = 0;
                var clear_counter = function () {
                    touch_counter = 0;
                    window.clearTimeout(on_touch_timer);
                };
                var add_touches = function (event) {
                    // No hacer nada.
                };
                var remove_touches = function (event) {
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 0) {
                        clear_counter();
                        return;
                    }
                    touch_counter += 1;
                    if (touch_counter == 2) {
                        onDoubleTouch(element);
                        clear_counter();
                        return;
                    }
                    window.setTimeout(clear_counter, 250);
                };
                var move_touches = function (event) {
                    // No hacer nada.
                };
                _this.gesturesHelper.addTouchEvents(element, add_touches, remove_touches, move_touches);
            }),
            /**
             * Funcion onRotate
             * @memberof LSF.gestures
             * @function onRotate
             * @param {Element} elemento Elemento a ejecutarse el gesto
             * @param {callback} onScroll Funcion que se ejecuta al realizar el gesto devolviendo el elemento y las diferencias del angulo original con el actual
             * @example
             * var elemento = document.getElementById("prueba");
             * LSF.gestures.onDeselect(elemento,function(el,angle){
             *  el.innerHTML = "El elemento ha rotado " + angle;
             * })
             */
            onRotate: (function (element, onRotate) {
                var touches = [];
                var timer = null, enabled = false;
                var center_point = null;
                var last_points = null;
                var get_points = function () {
                    var points = [];
                    for (var i = 0; i < touches.length; i++)
                        points.push({
                            pageX: touches[i].pageX,
                            pageY: touches[i].pageY
                        });
                    return points;
                };
                var calculate_diff = function () {
                    var angle = function (p0, p1, p2) {
                        var v1, v2;
                        // Obtener el vector 1 y el vector 2.
                        v1 = { x: p0.pageX - p1.pageX, y: p0.pageY - p1.pageY };
                        v2 = { x: p2.pageX - p1.pageX, y: p2.pageY - p1.pageY };
                        // Obtener la diferencia de ángulos.
                        return Math.atan2(v2.y, v2.x) - Math.atan2(v1.y, v1.x);
                    };
                    var a = last_points;
                    var b = get_points();
                    var t = Math.abs(angle(b[0], center_point, b[1]));
                    if (3.0 <= t && t <= 3.3)
                        return null;
                    return angle(b[0], center_point, b[1]) - angle(a[0], center_point, a[1]);
                };
                var clear = function () {
                    window.clearTimeout(timer);
                    center_point = null;
                    last_points = null;
                    enabled = false;
                    _this.gesturesHelper.currentGesture.rotate = false;
                };
                var add_touches = function (event) {
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 2)
                        return clear();
                    timer = window.setTimeout(function () {
                        clear();
                    }, 200);
                    last_points = get_points();
                    center_point = {
                        pageX: (last_points[0].pageX + last_points[1].pageX) / 2.0,
                        pageY: (last_points[0].pageY + last_points[1].pageY) / 2.0
                    };
                };
                var remove_touches = function (event) {
                    clear();
                };
                var move_touches = function (event) {
                    if (_this.gesturesHelper.currentGesture.scroll || _this.gesturesHelper.currentGesture.zoom)
                        return clear();
                    if (last_points == null)
                        return clear();
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 2) {
                        last_points = null;
                        return clear();
                    }
                    var diff_rad = calculate_diff();
                    if (!enabled && diff_rad == null)
                        return;
                    if (!enabled) {
                        window.clearTimeout(timer);
                        enabled = true;
                    }
                    _this.gesturesHelper.currentGesture.rotate = true;
                    last_points = get_points();
                    onRotate(element, diff_rad / 1.5);
                };
                _this.gesturesHelper.addTouchEvents(element, add_touches, remove_touches, move_touches);
            }),
            /**
             * Function setBackground
             * @memberof LSF.gestures
             * @function setBackground
             * @param {Element} elemento Elemento a colocarse como elemento Base
             * @example
             * var elemento = document.getElementById("prueba");
             * LSF.gestures.setBackground(elemento)
             */
            setBackground: (function (element) {
                var touches = [];
                var add_touches = function (event) {
                    touches = _this.gesturesHelper.getTouches(event, element);
                    if (touches.length != 1) {
                        return;
                    }
                    var selected = document.querySelectorAll('[data-selected="1"]');
                    for (var i = 0; i < selected.length; i++) {
                        var e = selected[i];
                        e.dataset['selected'] = '0';
                        e.dataset['firstSelected'] = '0';
                        if (typeof e['gestureEventOnDeselect'] == 'function')
                            e['gestureEventOnDeselect'](e);
                    }
                };
                var remove_touches = function (event) {
                    // No hacer nada.
                };
                var move_touches = function (event) {
                    // No hacer nada.
                };
                _this.gesturesHelper.addTouchEvents(element, add_touches, remove_touches, move_touches);
            })
        };
        /**
         * Modulo que permite gestionar conexiones asincronas en tiempo real mediante WebSockets y WebRTC
         * @namespace {lsf_connectivityModule} connectivity
         * @memberof LSF
         */
        this.connectivity = {
            /**
             * Función que permite saber si existe conexiones WebSockets Activas que el framework esté usando.
             * @memberof LSF.connectivity
             * @function isWsActive
             * @returns {true | false} Devuelve true si la conexión si existe conexión con websockets, que esté activa y si se ha encontrado la IP Local, de lo contrario devuelve false
             * @example
             * LSF.connectivity.isWsActive();
             */
            isWsActive: function () {
                return _this.webSocket.isActive();
            },
            /**
             * Función que permite descubrir servidores activos en la red local bajo un puerto determinado, y saber si éstos estan disponibles para poder establecer una conexión
             * @memberof LSF.connectivity
             * @function discoveryServers
             * @param {number} port Puerto al cual se desea escuchar al servidor
             * @param {callback} onDiscovery Callback que se ejecutará cada vez que encuentre un servidor compatible
             * @example
             * LSF.connectivity.discoveryServers(8080,function(){
             * console.log("Se ha encontrado un servidor");
             * })
             */
            discoveryServers: function (port, onDiscovery) {
                if (_this.webSocket.tools.localIPReady) {
                    var clone = {};
                    clone = $.extend(_this.webSocket.connectionConfig, {
                        host: _this.device.ipAddress,
                        port: port,
                        keepAlive: 0
                    });
                    _this.webSocket.tools.discoveryServers(clone, onDiscovery);
                }
                else {
                    _this.log.error("Realize primero la deteccion de la IP local antes de continuar.");
                }
            },
            /**
             * Función que establece una conexión WebSocket contra un servidor
             * @function connect
             * @memberof LSF.connectivity
             * @param {_lsf_connectionConfig} config Objeto que se envia para poder establecer la conexión
             * <pre>
             * {
             *  host: string, //IP del host
             *  port: number, //Puerto de conexion
             *  keepAlive?: number //ping en milimetros para mantener activa la conexion
             * }
             * </pre>
             * <sub> El simbolo ? significa que el parametro es opcional </sub>
             */
            connect: function (config, onSuccess, onError, onClose) {
                var clone = {};
                clone = $.extend(_this.webSocket.connectionConfig, config);
                _this.webSocket.establishConnection(clone, onSuccess, onError, onClose);
            },
            /**
             * Función que permite cerrar las conexiones activas de WebSocket y WebRTC, retorna true si se ha desconectado sin incidentes, false si ocurre lo contrario
             * @memberof LSF.connectivity
             * @function disconnect
             * @returns {true | false}
             */
            diconnect: function () {
                try {
                    _this.webSocket.connection.close();
                    return true;
                }
                catch (e) {
                    _this.log.error(e);
                    return false;
                }
            },
            /**
             * SubModulo que permite enviar los mensajes al servidor
             * @memberof LSF.connectivity
             * @function send
             * @prop {function} send.message función que permite enviar una trama message, tiene como parametros <code>(data , targetDevices)</code> donde <code>data</code> es el mensaje a enviar, y <code>targetDevices</code> es un Array de a quienes se les envía el mensaje, si se omite se realiza un broadcast (envío a todos los nodos conectados del servidor).
             * @prop {function} send.error funcion que permite enviar una trama de errores (pila de eventos o log de errores) tiene como parametros <code>(data , stackTrace, targetDevices)</code> donde <code>data</code> es el nombre del error a enviar, <code>stackTrace</code> es la sección para mandar la pila del evento o el mensaje de error y <code>targetDevices</code> es un Array de a quienes se les envía el mensaje, si se omite se realiza un broadcast (envío a todos los nodos conectados del servidor).
             * @prop {function} send.event funcion que permite enviar una trama tipo evento, se usa cuando se quiere disparar una función desde un dispositivo remoto tiene como parametros <code>(eventName, data , targetDevices)</code> donde <code>eventName</code> es el nombre del evento a enviar, <code> y <code>targetDevices</code> es un Array de a quienes se les envía el mensaje, si se omite se realiza un broadcast (envío a todos los nodos conectados del servidor).
             */
            send: {
                message: function (data, targetDevices) {
                    if (_this.connectivity.isWsActive()) {
                        try {
                            var messageplot = {
                                command: _this.commandsName.message,
                                messageData: data,
                                from: _this.device,
                                targetDevices: targetDevices || []
                            };
                            _this.webSocket.connection.send(JSON.stringify(messageplot));
                        }
                        catch (e) {
                            _this.log.error(e);
                        }
                    }
                    else {
                        _this.log.error("No se encuentra activa la conexion, por favor intenta conectarte al servidor primero.");
                    }
                },
                error: function (data, stackTrace, targetDevices) {
                    if (_this.connectivity.isWsActive()) {
                        try {
                            var errorplot = {
                                command: _this.commandsName.error,
                                errorData: data,
                                from: _this.device,
                                stackTrace: stackTrace || "",
                                targetDevices: targetDevices || []
                            };
                            _this.webSocket.connection.send(JSON.stringify(errorplot));
                        }
                        catch (e) {
                            _this.log.error(e);
                        }
                    }
                    else {
                        _this.log.error("No se encuentra activa la conexion, por favor intenta conectarte al servidor primero.");
                    }
                },
                event: function (eventName, data, targetDevices) {
                    if (_this.connectivity.isWsActive()) {
                        try {
                            var eventplot = {
                                command: _this.commandsName.event,
                                eventData: data,
                                eventName: eventName,
                                from: _this.device,
                                targetDevices: targetDevices || []
                            };
                        }
                        catch (e) {
                            _this.log.error(e);
                        }
                    }
                    else {
                        _this.log.error("No se encuentra activa la conexion, por favor intenta conectarte al servidor primero.");
                    }
                }
            },
            /**
             * Objeto en el cual se le pueden agregar callbacks para cada tipo de eventos descritos en LSF.commandsName y que recibe un parametro plot, que obtiene la trama que envía el servidor
             * @memberof LSF.connectivity
             * @example
             * LSF.connectivity.userCallbacks[LSF.commandsName.message] = function(plot){
             *   //funcion se ejecutara cuando llegue una trama tipo message desde el servidor
             *   //toda la información se obtendrá a traves del parametro plot
             * }
             *
             *  */
            userCallbacks: {}
        };
    }
    return LSF;
}());
//# sourceMappingURL=lsf.js.map