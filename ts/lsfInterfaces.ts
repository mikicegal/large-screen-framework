namespace largeScreenFramework {

    export type Events = "resultstart" |
        "error" |
        "end" |
        "result" |
        "resultMatch" |
        "resultNoMatch" |
        "errorNetwork" |
        "errorPermissionBlocked" |
        "errorPermissionDenied";

    /**
     * Interfaces privadas.
     */

    export interface _lsf_debug {
        trace: ((parameters: any) => any),
        info: ((parameters: any) => any),
        warn: ((parameters: any) => any),
        error: ((parameters: any) => any)
    }

    export interface _lsf_commandsName {
        message: string,
        error: string,
        event: string,
        webSocket: {
            discoveryServers: {
                request: string,
                response: string,
            },
            updateDevicesInfo: {
                request: string,
                response: string
            },
            getUniqueId: {
                request: string,
                response: string
            },
            sendDevice: string,
        }
    }

    export interface _lsf_connectionConfig {
        host: string,
        usingSecure?: boolean,
        protocol?: {
            secure: string,
            insecure: string
        },
        port: number,
        keepAlive?: number
    }

    export interface _lsf_webSocketModule_tools {
        localIPReady: boolean,
        avaliableServers: Array<{ url: string, status: string }>,
        findLocalIP(success: ((ip: string) => any), error: ((err: string) => any)): void,
        discoveryServers(config: _lsf_connectionConfig, onDiscovery: ((statusPlot: _lsf_plot_custom) => any)): void,

    }

    export interface _lsf_webSocketModule {
        connection: WebSocket | any,
        isActive: (() => boolean),
        connectionConfig: _lsf_connectionConfig,
        establishConnection(config: _lsf_connectionConfig, success: (() => any), error?: ((err: string) => any), close?: ((reason: string) => any)): void,
        openConnection(success: (() => any), error?: ((err: string) => any), close?: ((reason: string) => any)): void,
        tools: _lsf_webSocketModule_tools
    }

    export interface _lsf_webRTCModule {

    }

    export interface _lsf_speechRecognitionModule {
        setLanguage: ((language: string) => any),
        addCallback: ((index: Events, callback: () => any) => any),
        addCommand: ((command: string, callback: () => any) => any),
        start: (() => any),
        commands: { [key: string]: () => any },
    }

    export interface _lsf_gesturesModule {
        onZoom: ((element: Element, onZoom: ((element: Element, diff_z: number) => void))=>void),
        onSelect: ((element: Element, onSelect: ((element: Element) => void))=>void),
        onDeselect: ((element: Element, onDeselect: ((element: Element) => void))=>void),
        onDragAndDrop: ((element: Element, onDrag: ((element: Element, diff_x: number, diff_y: number) => void), onDrop: ((element: Element) => void))=>void),
        onScroll: ((element: Element, onScroll: ((element: Element, diff_x: number, diff_y: number) => void))=>void),
        onDoubleTouch: ((element: Element, onDoubleTouch: ((element: Element) => void))=>void),
        onRotate: ((element: Element, onRotate: ((element: Element, diff_a: number) => void))=>void),
        setBackground: ((element: Element)=>void)
    }

    export interface _lsf_gesturesHelper {
        getTouches: ((event: Event, element: Element)=>Array<Touch>),
        addTouchEvents: ((element: Element, add_touches: ((event: Event) => void), remove_touches: ((event: Event) => void), move_touches: ((event: Event) => void))=>void),
        currentGesture: {
            scroll: boolean,
            rotate: boolean,
            zoom: boolean,
            select: {
                multiple: boolean
            }
        }
    }

    export interface _lsf_workspacesModule {
        callbacksByKeyboard: {[index:string]:any},
        setCallbackByKeyboard(workspace:string,callback:(keypress:string)=>any):void,
        initializeWorkspace: ((element: any)=>boolean),
        registerUser: ((element: any, user: string)=>boolean),
        setWorkspaceColor: ((element: any, color: string)=>boolean),
        sharedWorkspaces: boolean,
        rotateWorkspace: ((element: any, angle: number)=>boolean)
    }

    export interface _lsf_virtualKeyboard{
        create: ((workspace:string)=>boolean),
        toggleView:((workspace:string)=>any),
        toggleCases:((workspace:string)=>any),
        delete:((workspace:string)=>boolean),
        caps:{[index:string]:boolean}
    }
    
    export interface _lsf_plot_device {
        uniqueId: number,
        friendlyName: string,
        ipAddress: string,
        platform: string,
        longitude: string,
        latitude: string
    }

    export interface _lsf_plot_message {
        command: string,
        messageData: string,
        from: _lsf_plot_device,
        targetDevices?: Array<string>
    }

    export interface _lsf_plot_error {
        command: string,
        errorData: string,
        from: _lsf_plot_device,
        stackTrace?: string,
        targetDevices?: Array<string>
    }

    export interface _lsf_plot_event {
        command: string,
        eventName: string,
        eventData: string,
        from: _lsf_plot_device,
        targetDevices?: Array<string>
    }

    export interface _lsf_plot_custom {
        command: string,
        customData?: any,
        from?: string,
        targetDevices?: Array<string>
    }

    /**
     * Interfaces públicas.
     */

    export interface lsf_plot_data {
        data: any,
        targetDevices?: Array<string>
    }

    export interface lsf_send_connectivity {
        message: ((data: string, targetDevices?: Array<string>) => any),
        error: ((data: string, stackTrace?: string, targetDevices?: Array<string>) => any),
        event: ((eventName: string, data: string, targetDevices?: Array<string>) => any)
    }

    export interface lsf_connectivityModule {
        isWsActive: (() => boolean),
        discoveryServers: ((port: number, onDiscovery: () => any) => any),
        connect: ((config: _lsf_connectionConfig, success: (() => any), error?: ((err: string) => any), close?: ((reason: string) => any)) => any),
        diconnect: (() => any),
        send: lsf_send_connectivity,
        userCallbacks: { [id: string]: (plot: any) => any }
    }
    
    export interface lsf_workspaces {
        onKeyBoardPress(idWorkspace,callback):void,
        moveKeyBoard(idWorkspace,posx,posy):void
    }
}
