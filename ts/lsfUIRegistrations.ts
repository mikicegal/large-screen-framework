
/*
* REGISTRO DE CUSTOM ELEMENTS HTML5 DEL FRAMEWORK
*/

document.registerElement('lsf-button-float');
document.registerElement('lsf-button');
document.registerElement('lsf-button-flat');
document.registerElement('lsf-card');
document.registerElement('lsf-modal');
document.registerElement('lsf-modal-dialog');
document.registerElement('lsf-tabs');
document.registerElement('lsf-tab');
document.registerElement('lsf-switch');
document.registerElement('lsf-notification');
document.registerElement('lsf-workspaces');
document.registerElement('lsf-keyboard');
$("lsf-button").each(function () {
  $(this).addClass("palette-" + $(this).attr("color") + " bg");
});

function toggleModal(id) {
  if ($(id).is(":visible")) {
    $(id).fadeOut(500);
  } else {
    $(id).fadeIn(500);

    setTimeout(function () {
      $(id + " > lsf-modal-dialog").css("height", $(id + " > lsf-modal-dialog").outerHeight() + "px");
      $(id + " > lsf-modal-dialog").css("bottom", "0");
    }, 500);
  }
}

