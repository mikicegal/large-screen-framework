Large Screen Surfaces
=====================

1. Objeto de Estudio
---------------------

* El objeto de estudio del presente proyecto es la tecnología del Large Screen Surfaces, el cual es definido por Gartner como pantallas de gran tamaño que soportan la interacción directa mediante el tacto o gestos. Estas pantallas pueden estar en orientación tanto vertical como horizontal e incorporan elementos de interacción multitouch que se encuentran en dispositivos “de mano” como los smartphones o tablets, pero a diferencia de éstos, reconocen múltiples usuarios, permitiendo realizar trabajos colaborativos. El tamaño está
restringido a la habilidad de físicamente alcanzar toda la superficie. Las pantallas más grandes usualmente soportan una interface gestual, donde el usuario no necesita físicamente tocar la superficie. (GARTNER, 2012).
Dentro del Large Screen Surfaces, el proyecto estudiará el desarrollo de aplicaciones, específicamente del tipo empresarial que permite el trabajo colaborativo e interactúan con el dispositivo. También abarca el análisis de la experiencia de usuario, definido como la experiencia que una persona obtiene cuando él / ella interactúa con un producto en condiciones particulares como factores sociales, culturales y el contexto del uso del producto. (Arhippainen &amp; Tähti, 2003).


2. Dominio del Problema
-----------------------

* El desarrollo en dispositivos con pantallas de gran tamaño se remonta a la década de los 90’, donde se comienza a evidenciar las primeras investigaciones sobre interacciones con superficies, como el caso de la Digital Desk, un intento de fusionar el mundo físico con el mundo electrónico, dando como resultado una mesa digital donde el usuario puede manipular objetos físicos y electrónicos cambiando de esta manera una estación de trabajo convencional (Wellner, 1991); pero debido a las limitantes de ese tiempo, como la baja
 resolución de los dispositivos, baja capacidad de procesamiento e interactividad touch limitada, las aplicaciones para esta tecnología quedaron relegadas varios años. Es en ésta última década donde se ha comenzado a realizar grandes avances en este campo, pudiendo observar actualmente resoluciones 4K, soporte multitouch de 100 puntos de toque, procesamiento multihilo, tarjetas gráficas potentes (MICROSOFT, 2015); lo que ha permitido a las empresas poder crear soluciones innovadoras dentro del ámbito
empresarial.
A raíz de este crecimiento, cada empresa desarrolla sus aplicaciones en base a frameworks creados para dicho propósito y, por ende, si una persona desea desarrollar una aplicación para Large Screen Surfaces debe de seleccionar el dispositivo en el que se desplegará puesto que no es lo mismo desarrollar para otro distinto. Cada framework de desarrollo está enfocado exclusivamente en el dispositivo de cada fabricante y eso limita el despliegue de cada aplicación y su alcance. Además, cada vez que un equipo de desarrollo desee desarrollar la aplicación en un dispositivo donde no han desplegado anteriormente, deberán de pasar por una etapa de aprendizaje del framework de dicho dispositivo. Por esta razón, se busca brindar una opción de desarrollo que permita mitigar las limitaciones existentes en el desarrollo de aplicaciones para Large Screen Surfaces.


3. Planteamiento de la Solución
--------------------------------

* La solución que el proyecto plantea es crear y desarrollar un framework cross-platform que permita facilitar el desarrollo de aplicaciones, de manera que la aplicación pueda desplegarse en variedad de plataformas, ahorrando recursos en el desarrollo de una aplicación y darle la opción de incrementar su alcance.
Dicho framework contemplará también lineamientos de diseño, resultante de un análisis previamente realizado, con lo cual se busca asegurar que las aplicaciones realizadas con el framework están desarrolladas teniendo como centro a la experiencia del usuario. 
